package handler

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"kulit/crud/model"
	"net/http"
	"strconv"
	"strings"
)

var DB *sql.DB

func RegistDB(db *sql.DB) {
	DB = db
}

const (
	gejalasakit = "gejalasakit"
	penyakit    = "penyakit"
	riwayat     = "riwayat"
	usertb      = "usertb"
	info        = "info"
	hasildetail = "hasildetail"
	login       = "login"
	hasil       = "hasil"
)

func API(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	url := r.URL.Path
	dataParams := r.URL.Query().Get("params")
	dataURL := strings.Split(fmt.Sprintf("%v", url), "/")
	lastIndex := dataURL[len(dataURL)-1]
	fmt.Println(dataURL)
	fmt.Println(r.Method)
	switch dataURL[2] {
	case gejalasakit:
		switch r.Method {
		case http.MethodGet:
			if lastIndex == gejalasakit || lastIndex == "" {
				data, err := model.GetGejalasakit(DB, dataParams)
				if err != nil {
					http.Error(w, err.Error(), http.StatusBadRequest)
				}
				jsonData, err := json.Marshal(data)
				if err != nil {
					http.Error(w, err.Error(), http.StatusBadRequest)
				}
				w.Write(jsonData)
			} else {
				id, err := strconv.Atoi(lastIndex)
				if err != nil {
					http.Error(w, err.Error(), http.StatusBadRequest)
				}
				data := &model.Gejalasakit{Id_gejala: id}
				err = data.Get(DB)
				if err != nil {
					http.Error(w, err.Error(), http.StatusBadRequest)
				}
				fmt.Println("bla bla bla")
				fmt.Println(data)
				jsonData, err := json.Marshal(data)
				if err != nil {
					http.Error(w, err.Error(), http.StatusBadRequest)
				}
				w.Write(jsonData)
			}
		case http.MethodDelete:
			id, err := strconv.Atoi(lastIndex)
			if err != nil {
				http.Error(w, err.Error(), http.StatusBadRequest)
			}
			if lastIndex != gejalasakit {
				data := model.Gejalasakit{Id_gejala: id}
				err := data.Delete(DB)
				if err != nil {
					http.Error(w, err.Error(), http.StatusBadRequest)
				}
				w.Write([]byte("ok"))
			}
		case http.MethodPost:
			if lastIndex == gejalasakit {
				defer r.Body.Close()
				body, err := ioutil.ReadAll(r.Body)
				if err != nil {
					http.Error(w, err.Error(), http.StatusBadRequest)
				}
				var gejalasakit model.Gejalasakit
				err = json.Unmarshal(body, &gejalasakit)
				if err != nil {
					http.Error(w, err.Error(), http.StatusBadRequest)
				}

				err = gejalasakit.Insert(DB)
				if err != nil {
					http.Error(w, err.Error(), http.StatusBadRequest)
				}
			}

		case http.MethodPut:
			if lastIndex != gejalasakit {
				defer r.Body.Close()
				body, err := ioutil.ReadAll(r.Body)
				if err != nil {
					http.Error(w, err.Error(), http.StatusBadRequest)
				}
				jsonMap := make(map[string]interface{})
				err = json.Unmarshal(body, &jsonMap)
				if err != nil {
					http.Error(w, err.Error(), http.StatusBadRequest)
				}
				fmt.Println(jsonMap)
				id, err := strconv.Atoi(lastIndex)
				if err != nil {
					http.Error(w, err.Error(), http.StatusBadRequest)
				}
				data := model.Gejalasakit{Id_gejala: id}
				err = data.Update(DB, jsonMap)
				if err != nil {
					http.Error(w, err.Error(), http.StatusBadRequest)
					w.Write([]byte("OK"))
				}
			}
		default:
			fmt.Println("Salah Method")
		}
		//Penyakit
	case penyakit:
		switch r.Method {
		case http.MethodGet:
			if lastIndex == penyakit || lastIndex == "" {
				data2, err := model.GetPenyakit(DB, dataParams)
				if err != nil {
					http.Error(w, err.Error(), http.StatusBadRequest)
				}
				jsonData, err := json.Marshal(data2)
				if err != nil {
					http.Error(w, err.Error(), http.StatusBadRequest)
				}
				w.Write(jsonData)
			} else {
				id, err := strconv.Atoi(lastIndex)
				if err != nil {
					http.Error(w, err.Error(), http.StatusBadRequest)
				}
				data2 := &model.Penyakit{Id_penyakit: id}
				err = data2.Get(DB)
				if err != nil {
					http.Error(w, err.Error(), http.StatusBadRequest)
				}
				fmt.Println("bla bla bla")
				fmt.Println(data2)
				jsonData, err := json.Marshal(data2)
				if err != nil {
					http.Error(w, err.Error(), http.StatusBadRequest)
				}
				w.Write(jsonData)
			}
		case http.MethodDelete:
			id, err := strconv.Atoi(lastIndex)
			if err != nil {
				http.Error(w, err.Error(), http.StatusBadRequest)
			}
			if lastIndex != penyakit {
				data2 := model.Penyakit{Id_penyakit: id}
				err := data2.Delete(DB)
				if err != nil {
					http.Error(w, err.Error(), http.StatusBadRequest)
				}
				w.Write([]byte("ok"))
			}
		case http.MethodPost:
			if lastIndex == penyakit {
				defer r.Body.Close()
				body, err := ioutil.ReadAll(r.Body)
				if err != nil {
					http.Error(w, err.Error(), http.StatusBadRequest)
				}
				var penyakit model.Penyakit
				err = json.Unmarshal(body, &penyakit)
				if err != nil {
					http.Error(w, err.Error(), http.StatusBadRequest)
				}

				err = penyakit.Insert(DB)
				if err != nil {
					http.Error(w, err.Error(), http.StatusBadRequest)
				}
			}

		case http.MethodPut:
			if lastIndex != penyakit {
				defer r.Body.Close()
				body, err := ioutil.ReadAll(r.Body)
				if err != nil {
					http.Error(w, err.Error(), http.StatusBadRequest)
				}
				jsonMap := make(map[string]interface{})
				err = json.Unmarshal(body, &jsonMap)
				if err != nil {
					http.Error(w, err.Error(), http.StatusBadRequest)
				}
				fmt.Println(jsonMap)
				id, err := strconv.Atoi(lastIndex)
				if err != nil {
					http.Error(w, err.Error(), http.StatusBadRequest)
				}
				data2 := model.Penyakit{Id_penyakit: id}
				err = data2.Update(DB, jsonMap)
				if err != nil {
					http.Error(w, err.Error(), http.StatusBadRequest)
					w.Write([]byte("OK"))
				}
			}
		default:
			fmt.Println("Salah Method")
		}

		//riwayat
	case riwayat:
		switch r.Method {
		case http.MethodGet:
			if lastIndex == riwayat || lastIndex == "" {
				data3, err := model.GetRiwayat(DB, dataParams)
				if err != nil {
					http.Error(w, err.Error(), http.StatusBadRequest)
				}
				jsonData, err := json.Marshal(data3)
				if err != nil {
					http.Error(w, err.Error(), http.StatusBadRequest)
				}
				w.Write(jsonData)
			} else {
				id, err := strconv.Atoi(lastIndex)
				if err != nil {
					http.Error(w, err.Error(), http.StatusBadRequest)
				}
				data3 := &model.Riwayat{Id_riwayat: id}
				err = data3.Get(DB)
				if err != nil {
					http.Error(w, err.Error(), http.StatusBadRequest)
				}
				jsonData, err := json.Marshal(data3)
				if err != nil {
					http.Error(w, err.Error(), http.StatusBadRequest)
				}
				w.Write(jsonData)
			}
		case http.MethodDelete:
			id, err := strconv.Atoi(lastIndex)
			if err != nil {
				http.Error(w, err.Error(), http.StatusBadRequest)
			}
			if lastIndex != riwayat {
				data3 := model.Riwayat{Id_riwayat: id}
				err := data3.Delete(DB)
				if err != nil {
					http.Error(w, err.Error(), http.StatusBadRequest)
				}
				w.Write([]byte("ok"))
			}
		case http.MethodPost:
			if lastIndex == riwayat {
				defer r.Body.Close()
				body, err := ioutil.ReadAll(r.Body)
				if err != nil {
					http.Error(w, err.Error(), http.StatusBadRequest)
				}
				var riwayat model.Riwayat
				err = json.Unmarshal(body, &riwayat)
				if err != nil {
					http.Error(w, err.Error(), http.StatusBadRequest)
				}

				err = riwayat.Insert(DB)
				if err != nil {
					http.Error(w, err.Error(), http.StatusBadRequest)
				}
			}

		case http.MethodPut:
			if lastIndex != riwayat {
				defer r.Body.Close()
				body, err := ioutil.ReadAll(r.Body)
				if err != nil {
					http.Error(w, err.Error(), http.StatusBadRequest)
				}
				jsonMap := make(map[string]interface{})
				err = json.Unmarshal(body, &jsonMap)
				if err != nil {
					http.Error(w, err.Error(), http.StatusBadRequest)
				}
				fmt.Println(jsonMap)
				id, err := strconv.Atoi(lastIndex)
				if err != nil {
					http.Error(w, err.Error(), http.StatusBadRequest)
				}
				data3 := model.Riwayat{Id_riwayat: id}
				err = data3.Update(DB, jsonMap)
				if err != nil {
					http.Error(w, err.Error(), http.StatusBadRequest)
					w.Write([]byte("OK"))
				}
			}
		default:
			fmt.Println("Salah Method")
		}

		//usertb
	case usertb:
		switch r.Method {
		case http.MethodGet:
			if lastIndex == usertb || lastIndex == "" {
				data4, err := model.GetUsertb(DB, dataParams)
				if err != nil {
					http.Error(w, err.Error(), http.StatusBadRequest)
				}
				jsonData, err := json.Marshal(data4)
				if err != nil {
					http.Error(w, err.Error(), http.StatusBadRequest)
				}
				w.Write(jsonData)
			} else {
				id, err := strconv.Atoi(lastIndex)
				if err != nil {
					http.Error(w, err.Error(), http.StatusBadRequest)
				}
				data4 := &model.Usertb{Id_user: id}
				err = data4.Get(DB)
				if err != nil {
					http.Error(w, err.Error(), http.StatusBadRequest)
				}
				fmt.Println("bla bla bla")
				fmt.Println(data4)
				jsonData, err := json.Marshal(data4)
				if err != nil {
					http.Error(w, err.Error(), http.StatusBadRequest)
				}
				w.Write(jsonData)
			}
		case http.MethodDelete:
			id, err := strconv.Atoi(lastIndex)
			if err != nil {
				http.Error(w, err.Error(), http.StatusBadRequest)
			}
			if lastIndex != usertb {
				data4 := model.Usertb{Id_user: id}
				err := data4.Delete(DB)
				if err != nil {
					http.Error(w, err.Error(), http.StatusBadRequest)
				}
				w.Write([]byte("ok"))
			}
		case http.MethodPost:
			if lastIndex == usertb {
				defer r.Body.Close()
				body, err := ioutil.ReadAll(r.Body)
				if err != nil {
					http.Error(w, err.Error(), http.StatusBadRequest)
				}
				var usertb model.Usertb
				err = json.Unmarshal(body, &usertb)
				if err != nil {
					http.Error(w, err.Error(), http.StatusBadRequest)
				}

				err = usertb.Insert(DB)
				if err != nil {
					http.Error(w, err.Error(), http.StatusBadRequest)
				}
			}

		case http.MethodPut:
			if lastIndex != usertb {
				defer r.Body.Close()
				body, err := ioutil.ReadAll(r.Body)
				if err != nil {
					http.Error(w, err.Error(), http.StatusBadRequest)
				}
				jsonMap := make(map[string]interface{})
				err = json.Unmarshal(body, &jsonMap)
				if err != nil {
					http.Error(w, err.Error(), http.StatusBadRequest)
				}
				fmt.Println(jsonMap)
				id, err := strconv.Atoi(lastIndex)
				if err != nil {
					http.Error(w, err.Error(), http.StatusBadRequest)
				}
				data4 := model.Usertb{Id_user: id}
				err = data4.Update(DB, jsonMap)
				if err != nil {
					http.Error(w, err.Error(), http.StatusBadRequest)
					w.Write([]byte("OK"))
				}
			}
		default:
			fmt.Println("Salah Method")
		}

		//Info
	case info:
		fmt.Println(r.Method)
		switch r.Method {
		case http.MethodGet:
			if lastIndex == info || lastIndex == "" {
				data5, err := model.GetInfo(DB, dataParams)
				if err != nil {
					http.Error(w, err.Error(), http.StatusBadRequest)
				}
				jsonData, err := json.Marshal(data5)
				if err != nil {
					http.Error(w, err.Error(), http.StatusBadRequest)
				}
				w.Write(jsonData)
			} else {
				id, err := strconv.Atoi(lastIndex)
				if err != nil {
					http.Error(w, err.Error(), http.StatusBadRequest)
				}
				data5 := &model.Info{Id_info: id}
				err = data5.Get(DB)
				if err != nil {
					http.Error(w, err.Error(), http.StatusBadRequest)
				}
				fmt.Println("bla bla bla")
				fmt.Println(data5)
				jsonData, err := json.Marshal(data5)
				if err != nil {
					http.Error(w, err.Error(), http.StatusBadRequest)
				}
				w.Write(jsonData)
			}
		case http.MethodDelete:
			id, err := strconv.Atoi(lastIndex)
			if err != nil {
				http.Error(w, err.Error(), http.StatusBadRequest)
			}
			if lastIndex != info {
				data5 := model.Info{Id_info: id}
				err := data5.Delete(DB)
				if err != nil {
					http.Error(w, err.Error(), http.StatusBadRequest)
				}
				w.Write([]byte("ok"))
			}
		case http.MethodPost:
			if lastIndex == info {
				defer r.Body.Close()
				body, err := ioutil.ReadAll(r.Body)
				if err != nil {
					http.Error(w, err.Error(), http.StatusBadRequest)
				}
				var info model.Info
				err = json.Unmarshal(body, &info)
				if err != nil {
					http.Error(w, err.Error(), http.StatusBadRequest)
				}
				fmt.Println(info)

				err = info.Insert(DB)
				if err != nil {
					http.Error(w, err.Error(), http.StatusBadRequest)
				}
			}

		case http.MethodPut:
			if lastIndex != info {
				defer r.Body.Close()
				body, err := ioutil.ReadAll(r.Body)
				if err != nil {
					http.Error(w, err.Error(), http.StatusBadRequest)
				}
				jsonMap := make(map[string]interface{})
				err = json.Unmarshal(body, &jsonMap)
				if err != nil {
					http.Error(w, err.Error(), http.StatusBadRequest)
				}
				fmt.Println(jsonMap)
				id, err := strconv.Atoi(lastIndex)
				if err != nil {
					http.Error(w, err.Error(), http.StatusBadRequest)
				}
				data5 := model.Info{Id_info: id}
				err = data5.Update(DB, jsonMap)
				if err != nil {
					http.Error(w, err.Error(), http.StatusBadRequest)
					w.Write([]byte("OK"))
				}
			}
		default:
			fmt.Println("Salah Method")
		}
		//hasildetail
	case hasildetail:
		switch r.Method {
		case http.MethodGet:
			if lastIndex == hasildetail || lastIndex == "" {
				data6, err := model.GetHasildetail(DB, dataParams)
				if err != nil {
					http.Error(w, err.Error(), http.StatusBadRequest)
				}
				jsonData, err := json.Marshal(data6)
				if err != nil {
					http.Error(w, err.Error(), http.StatusBadRequest)
				}
				w.Write(jsonData)
			} else {
				id, err := strconv.Atoi(lastIndex)
				if err != nil {
					http.Error(w, err.Error(), http.StatusBadRequest)
				}
				data6 := &model.Hasildetail{Id_pil: id}
				err = data6.Get(DB)
				if err != nil {
					http.Error(w, err.Error(), http.StatusBadRequest)
				}
				fmt.Println("bla bla bla")
				fmt.Println(data6)
				jsonData, err := json.Marshal(data6)
				if err != nil {
					http.Error(w, err.Error(), http.StatusBadRequest)
				}
				w.Write(jsonData)
			}
		case http.MethodDelete:
			id, err := strconv.Atoi(lastIndex)
			if err != nil {
				http.Error(w, err.Error(), http.StatusBadRequest)
			}
			if lastIndex != hasildetail {
				data6 := model.Hasildetail{Id_pil: id}
				err := data6.Delete(DB)
				if err != nil {
					http.Error(w, err.Error(), http.StatusBadRequest)
				}
				w.Write([]byte("ok"))
			}
		case http.MethodPost:
			if lastIndex == hasildetail || lastIndex == "" {
				defer r.Body.Close()
				body, err := ioutil.ReadAll(r.Body)
				if err != nil {
					http.Error(w, err.Error(), http.StatusBadRequest)
				}
				var hasildetail []*model.Hasildetail
				err = json.Unmarshal(body, &hasildetail)
				if err != nil {
					fmt.Println(err)
					http.Error(w, err.Error(), http.StatusBadRequest)
				}
				// fmt.Printf("body : \n%s\n", string(body[:]))
				// fmt.Printf("hasildetail handler : \n%s\n", hasildetail)
				params, ok := r.URL.Query()["id_user"]
				if !ok || len(params) < 1 {
					http.Error(w, err.Error(), http.StatusBadRequest)
				}
				// fmt.Println(len(params))
				// fmt.Println(params[0])
				// fmt.Println(hasildetail)
				// fmt.Printf("%v", hasildetail)

				id_user, _ := strconv.Atoi(params[0])
				user := &model.Usertb{
					Id_user: id_user,
				}
				// if err := user.Get(DB); err != nil {
				// 	http.Error(w, err.Error(), http.StatusInternalServerError)
				// }
				riwayat := &model.Riwayat{}
				riwayat.Id_user = user.Id_user
				// riwayat.Nama = user.Nama
				if err := riwayat.Insert(DB); err != nil {
					http.Error(w, err.Error(), http.StatusInternalServerError)
				}
				// for _, item := range hasildetail {
				// 	item.Id_riwayat = riwayat.Id_riwayat
				// 	if err := item.Insert(DB); err != nil {
				// 		http.Error(w, err.Error(), http.StatusBadRequest)
				// 	}
				// }
				// fmt.Printf("%s", hasildetail)
				// fmt.Println(model.HitHasildetail(hasildetail))
				// penyakit := model.HitHasildetail(hasildetail)
				// penyakit := &model.Penyakit{Id_penyakit: model.HitHasildetail(hasildetail)}
				// if err := penyakit.Get(DB); err != nil {
				// 	http.Error(w, err.Error(), http.StatusInternalServerError)
				// }

				hasil, err := model.HitHasildetail(DB, hasildetail)
				if err != nil {
					http.Error(w, err.Error(), http.StatusInternalServerError)
				}

				for _, item := range hasil {
					if err := item.Insert(DB); err != nil {
						http.Error(w, err.Error(), http.StatusBadRequest)
					}
					fmt.Println(item)
					for _, item2 := range item.Pilihan {
						item2.Id_hasil = item.Id_hasil
						// fmt.Printf("%v", item2)
						fmt.Println(item2)

						if err := item2.Insert(DB); err != nil {
							http.Error(w, err.Error(), http.StatusBadRequest)
						}
					}
				}

				// update := map[string]interface{}{
				// 	"penyakit": penyakit.Jenis_penyakit,
				// }
				// if err := riwayat.Update(DB, update); err != nil {
				// 	http.Error(w, err.Error(), http.StatusInternalServerError)
				// }
				fmt.Printf("testttttttt")
				fmt.Println(riwayat)
			}

		case http.MethodPut:
			if lastIndex != hasildetail {
				defer r.Body.Close()
				body, err := ioutil.ReadAll(r.Body)
				if err != nil {
					http.Error(w, err.Error(), http.StatusBadRequest)
				}
				jsonMap := make(map[string]interface{})
				err = json.Unmarshal(body, &jsonMap)
				if err != nil {
					http.Error(w, err.Error(), http.StatusBadRequest)
				}
				fmt.Println(jsonMap)
				id, err := strconv.Atoi(lastIndex)
				if err != nil {
					http.Error(w, err.Error(), http.StatusBadRequest)
				}
				data6 := model.Hasildetail{Id_pil: id}
				err = data6.Update(DB, jsonMap)
				if err != nil {
					http.Error(w, err.Error(), http.StatusBadRequest)
					w.Write([]byte("OK"))
				}
			}
		default:
			fmt.Println("Salah Method")
		}

		//LOGIN
	case login:
		fmt.Println(r.Method)
		switch r.Method {
		case http.MethodPost:
			fmt.Println(lastIndex)
			if lastIndex == login {
				defer r.Body.Close()
				body, err := ioutil.ReadAll(r.Body)
				if err != nil {
					http.Error(w, err.Error(), http.StatusBadRequest)
				}
				//untuk mengubah json menjadi struct
				var login model.Usertb
				err = json.Unmarshal(body, &login)
				if err != nil {
					http.Error(w, err.Error(), http.StatusBadRequest)
				}
				fmt.Println(login)
				//memasukkan data ke dalam database
				err = login.Login(DB)
				if err != nil {
					http.Error(w, err.Error(), http.StatusBadRequest)
				}
				fmt.Println(login)
				jsonData, err := json.Marshal(login)
				if err != nil {
					http.Error(w, err.Error(), http.StatusInternalServerError)
				}
				w.Write(jsonData)
			}
		default:
			fmt.Println("Salah Method")
		}

		//hasil_akhir
	case hasil:
		fmt.Println(r.Method)
		switch r.Method {
		case http.MethodGet:
			if lastIndex == hasil || lastIndex == "" {
				data7, err := model.GetHasil(DB, dataParams)
				if err != nil {
					http.Error(w, err.Error(), http.StatusBadRequest)
				}
				jsonData, err := json.Marshal(data7)
				if err != nil {
					http.Error(w, err.Error(), http.StatusBadRequest)
				}
				w.Write(jsonData)
			} else {
				id, err := strconv.Atoi(lastIndex)
				if err != nil {
					http.Error(w, err.Error(), http.StatusBadRequest)
				}
				data7 := &model.Hasil{Id_hasil: id}
				err = data7.Get(DB)
				if err != nil {
					http.Error(w, err.Error(), http.StatusBadRequest)
				}
				fmt.Println("bla bla bla")
				fmt.Println(data7)
				jsonData, err := json.Marshal(data7)
				if err != nil {
					http.Error(w, err.Error(), http.StatusBadRequest)
				}
				w.Write(jsonData)
			}
		case http.MethodDelete:
			id, err := strconv.Atoi(lastIndex)
			if err != nil {
				http.Error(w, err.Error(), http.StatusBadRequest)
			}
			if lastIndex != hasil {
				data7 := model.Hasil{Id_hasil: id}
				err := data7.Delete(DB)
				if err != nil {
					http.Error(w, err.Error(), http.StatusBadRequest)
				}
				w.Write([]byte("ok"))
			}
		case http.MethodPost:
			if lastIndex == hasil {
				defer r.Body.Close()
				body, err := ioutil.ReadAll(r.Body)
				if err != nil {
					http.Error(w, err.Error(), http.StatusBadRequest)
				}
				var hasil model.Hasil
				err = json.Unmarshal(body, &hasil)
				if err != nil {
					http.Error(w, err.Error(), http.StatusBadRequest)
				}
				fmt.Println(hasil)

				err = hasil.Insert(DB)
				if err != nil {
					http.Error(w, err.Error(), http.StatusBadRequest)
				}
			}

		// case http.MethodPut:
		// 	if lastIndex != hasil {
		// 		defer r.Body.Close()
		// 		body, err := ioutil.ReadAll(r.Body)
		// 		if err != nil {
		// 			http.Error(w, err.Error(), http.StatusBadRequest)
		// 		}
		// 		jsonMap := make(map[string]interface{})
		// 		err = json.Unmarshal(body, &jsonMap)
		// 		if err != nil {
		// 			http.Error(w, err.Error(), http.StatusBadRequest)
		// 		}
		// 		fmt.Println(jsonMap)
		// 		id, err := strconv.Atoi(lastIndex)
		// 		if err != nil {
		// 			http.Error(w, err.Error(), http.StatusBadRequest)
		// 		}
		// 		data7 := model.Hasil{Id_hasil: id}
		// 		err = data7.Update(DB, jsonMap)
		// 		if err != nil {
		// 			http.Error(w, err.Error(), http.StatusBadRequest)
		// 			w.Write([]byte("OK"))
		// 		}
		// 	}
		default:
			fmt.Println("Salah Method")
		}
	}
}
