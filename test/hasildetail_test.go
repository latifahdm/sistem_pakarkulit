package test

import (
	"fmt"
	"kulit/crud/lib"
	"kulit/crud/model"
	"testing"
)

var data6 = []*model.Hasildetail{
	&model.Hasildetail{Id_riwayat: 2, Id_penyakit: 1, Id_hasil: 4, Nama_gejala: "Gatal", Cf_pakar: 0.7},
	&model.Hasildetail{Id_riwayat: 3, Id_penyakit: 2, Id_hasil: 5, Nama_gejala: "Kering", Cf_pakar: 0.8},
}

func TestHasildetail(t *testing.T) {
	t.Run("Test Insert", func(t *testing.T) {
		db, err := lib.ConnectMysql(databaseTest)
		defer db.Close()
		if err != nil {
			t.Fatal(err)
		}

		for _, val := range data6 {
			err = val.Insert(db)
			if err != nil {
				t.Fatal(err)
			}
		}
	})

	t.Run("Test Update", func(t *testing.T) {
		db, err := lib.ConnectMysql(databaseTest)
		defer db.Close()
		if err != nil {
			t.Fatal(err)
		}

		dataUpdate := map[string]interface{}{
			"Nama_gejala": "Kering bersisik",
		}

		err = data6[1].Update(db, dataUpdate)
		if err != nil {
			t.Fatal(err)
		}
	})

	t.Run("Test Delete", func(t *testing.T) {
		db, err := lib.ConnectMysql(databaseTest)
		defer db.Close()
		if err != nil {
			t.Fatal(err)
		}
		data6 := model.Hasildetail{Id_pil: 2}
		err = data6.Delete(db)
		if err != nil {
			t.Fatal(err)
		}
	})

	t.Run("Test Get", func(t *testing.T) {
		db, err := lib.ConnectMysql(databaseTest)
		defer db.Close()
		if err != nil {
			t.Fatal(err)
		}
		data6 := model.Hasildetail{Id_pil: 3}
		err = data6.Delete(db)
		if err != nil {
			t.Fatal(err)
		}
		fmt.Println(data6)
	})

	t.Run("Test Gets", func(t *testing.T) {
		db, err := lib.ConnectMysql(databaseTest)
		defer db.Close()
		if err != nil {
			t.Fatal(err)
		}
		data6, err := model.GetHasildetail(db)
		if err != nil {
			t.Fatal(err)
		}
		for _, val := range data6 {
			fmt.Println(*val)
		}
	})
}
