package test

import (
	"bytes"
	"encoding/json"
	"fmt"
	"kulit/crud/handler"
	"kulit/crud/lib"
	"kulit/crud/model"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestRiwayatHandler(t *testing.T) {
	t.Run("Testing Insert", func(t *testing.T) {
		db, err := lib.ConnectMysql(databaseTest)
		defer db.Close()
		if err != nil {
			t.Fatal(err)
		}
		data3 := []model.Riwayat{
			{Penyakit: "cacar air"},
			{Penyakit: "Kudis"},
			{Penyakit: "panu"},
		}

		for _, val := range data3 {
			jsonData, err := json.Marshal(val)
			if err != nil {
				t.Fatal(err)
			}
			b := bytes.NewBuffer(jsonData)
			r := httptest.NewRequest(http.MethodPost, "/api/riwayat", b)
			w := httptest.NewRecorder()
			handler.RegistDB(db)
			handler.API(w, r)

			status := w.Code
			if status != http.StatusOK {
				t.Fatalf("get : %v want : %v status : %v", status, http.StatusOK, w.Body)
			}
		}
	})

	t.Run("Testing Update", func(t *testing.T) {
		db, err := lib.ConnectMysql(databaseTest)
		defer db.Close()
		if err != nil {
			t.Fatal(err)
		}
		update := map[string]interface{}{
			"nama": "eri",
		}

		jsonUpdate, err := json.MarshalIndent(update, "", "")
		if err != nil {
			t.Fatal(err)
		}
		b := bytes.NewBuffer(jsonUpdate)
		r := httptest.NewRequest(http.MethodPut, "/api/riwayat/2", b)
		w := httptest.NewRecorder()
		handler.RegistDB(db)
		handler.API(w, r)

		status := w.Code
		if status != http.StatusOK {
			t.Fatalf("get : %v want : %v status : %v", status, http.StatusOK, w.Body)
		}
	})

	t.Run("Testing Delete", func(t *testing.T) {
		db, err := lib.ConnectMysql(databaseTest)
		defer db.Close()
		if err != nil {
			t.Fatal(err)
		}
		r := httptest.NewRequest(http.MethodDelete, "/api/riwayat/1", nil)
		w := httptest.NewRecorder()
		handler.RegistDB(db)
		handler.API(w, r)

		status := w.Code
		if status != http.StatusOK {
			t.Fatalf("get : %v want : %v status : %v", status, http.StatusOK, w.Body)
		}
	})

	t.Run("Testing Gets", func(t *testing.T) {
		db, err := lib.ConnectMysql(databaseTest)
		defer db.Close()
		if err != nil {
			t.Fatal(err)
		}
		r := httptest.NewRequest(http.MethodGet, "/api/riwayat", nil)
		w := httptest.NewRecorder()
		handler.RegistDB(db)
		handler.API(w, r)

		status := w.Code
		if status != http.StatusOK {
			t.Fatalf("get : %v want : %v status : %v", status, http.StatusOK, w.Body)
		}

		var jsonData = []byte(w.Body.String())
		var riwayat []model.Riwayat

		err = json.Unmarshal(jsonData, &riwayat)
		if err != nil {
			t.Fatal(err)
		}
		fmt.Println(riwayat)
	})

	//getsWithParams
	t.Run("Testing GetsWithParams", func(t *testing.T) {
		db, err := lib.ConnectMysql(databaseTest)
		defer db.Close()
		if err != nil {
			t.Fatal(err)
		}
		r := httptest.NewRequest(http.MethodGet, "/api/riwayat?params=id_riwayat%2Clike%2C%251%25", nil)
		w := httptest.NewRecorder()
		handler.RegistDB(db)
		handler.API(w, r)

		status := w.Code
		if status != http.StatusOK {
			t.Fatalf("get : %v want : %v status : %v", status, http.StatusOK, w.Body)
		}

		var jsonData = []byte(w.Body.String())
		var riwayat []model.Riwayat

		err = json.Unmarshal(jsonData, &riwayat)
		if err != nil {
			t.Fatal(err)
		}
		fmt.Println(riwayat)
	})

	t.Run("Testing Get", func(t *testing.T) {
		db, err := lib.ConnectMysql(databaseTest)
		defer db.Close()
		if err != nil {
			t.Fatal(err)
		}
		r := httptest.NewRequest(http.MethodGet, "/api/riwayat/2", nil)
		w := httptest.NewRecorder()
		handler.RegistDB(db)
		handler.API(w, r)

		status := w.Code
		if status != http.StatusOK {
			t.Fatalf("get : %v want : %v status : %v", status, http.StatusOK, w.Body)
		}

		var jsonData = []byte(w.Body.String())
		var riwayat model.Riwayat

		err = json.Unmarshal(jsonData, &riwayat)
		if err != nil {
			t.Fatal(err)
		}
		fmt.Println(riwayat)
	})
}
