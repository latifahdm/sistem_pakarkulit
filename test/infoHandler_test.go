package test

import (
	"bytes"
	"encoding/json"
	"fmt"
	"kulit/crud/handler"
	"kulit/crud/lib"
	"kulit/crud/model"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestInfoHandler(t *testing.T) {
	t.Run("Testing Insert", func(t *testing.T) {
		db, err := lib.ConnectMysql(databaseTest)
		defer db.Close()
		if err != nil {
			t.Fatal(err)
		}
		data5 := []model.Info{
			{Judul: "Kutil"},
			{Judul: "Panu"},
			{Judul: "Cacar Air"},
		}

		for _, val := range data5 {
			jsonData, err := json.Marshal(val)
			if err != nil {
				t.Fatal(err)
			}
			b := bytes.NewBuffer(jsonData)
			r := httptest.NewRequest(http.MethodPost, "/api/info", b)
			w := httptest.NewRecorder()
			handler.RegistDB(db)
			handler.API(w, r)

			status := w.Code
			if status != http.StatusOK {
				t.Fatalf("get : %v want : %v status : %v", status, http.StatusOK, w.Body)
			}
		}
	})

	t.Run("Testing Update", func(t *testing.T) {
		db, err := lib.ConnectMysql(databaseTest)
		defer db.Close()
		if err != nil {
			t.Fatal(err)
		}
		data5 := model.Info{
			Judul: "Kudis",
		}
		jsonData, err := json.Marshal(data5)
		if err != nil {
			t.Fatal(err)
		}
		b := bytes.NewBuffer(jsonData)
		r := httptest.NewRequest(http.MethodPost, "/api/info/2", b)
		w := httptest.NewRecorder()
		handler.RegistDB(db)
		handler.API(w, r)

		status := w.Code
		if status != http.StatusOK {
			t.Fatalf("get : %v want : %v status : %v", status, http.StatusOK, w.Body)
		}
	})

	t.Run("Testing Delete", func(t *testing.T) {
		db, err := lib.ConnectMysql(databaseTest)
		defer db.Close()
		if err != nil {
			t.Fatal(err)
		}
		r := httptest.NewRequest(http.MethodDelete, "/api/info/1", nil)
		w := httptest.NewRecorder()
		handler.RegistDB(db)
		handler.API(w, r)

		status := w.Code
		if status != http.StatusOK {
			t.Fatalf("get : %v want : %v status : %v", status, http.StatusOK, w.Body)
		}
	})

	t.Run("Testing Gets", func(t *testing.T) {
		db, err := lib.ConnectMysql(databaseTest)
		defer db.Close()
		if err != nil {
			t.Fatal(err)
		}
		r := httptest.NewRequest(http.MethodGet, "/api/info", nil)
		w := httptest.NewRecorder()
		handler.RegistDB(db)
		handler.API(w, r)

		status := w.Code
		if status != http.StatusOK {
			t.Fatalf("get : %v want : %v status : %v", status, http.StatusOK, w.Body)
		}

		var jsonData = []byte(w.Body.String())
		var info []model.Info

		err = json.Unmarshal(jsonData, &info)
		if err != nil {
			t.Fatal(err)
		}
		fmt.Println(info)
	})

	//getsWithParams
	t.Run("Testing GetsWithParams", func(t *testing.T) {
		db, err := lib.ConnectMysql(databaseTest)
		defer db.Close()
		if err != nil {
			t.Fatal(err)
		}
		r := httptest.NewRequest(http.MethodGet, "/api/info?params=id_info%2Clike%2C%251%25", nil)
		w := httptest.NewRecorder()
		handler.RegistDB(db)
		handler.API(w, r)

		status := w.Code
		if status != http.StatusOK {
			t.Fatalf("get : %v want : %v status : %v", status, http.StatusOK, w.Body)
		}

		var jsonData = []byte(w.Body.String())
		var info []model.Info

		err = json.Unmarshal(jsonData, &info)
		if err != nil {
			t.Fatal(err)
		}
		fmt.Println(info)
	})

	t.Run("Testing Get", func(t *testing.T) {
		db, err := lib.ConnectMysql(databaseTest)
		defer db.Close()
		if err != nil {
			t.Fatal(err)
		}
		r := httptest.NewRequest(http.MethodGet, "/api/info/3", nil)
		w := httptest.NewRecorder()
		handler.RegistDB(db)
		handler.API(w, r)

		status := w.Code
		if status != http.StatusOK {
			t.Fatalf("get : %v want : %v status : %v", status, http.StatusOK, w.Body)
		}

		var jsonData = []byte(w.Body.String())
		var info model.Info

		err = json.Unmarshal(jsonData, &info)
		if err != nil {
			t.Fatal(err)
		}
		fmt.Println(info)
	})
}
