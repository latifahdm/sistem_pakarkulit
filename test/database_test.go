package test

import (
	"kulit/crud/lib"
	"kulit/crud/model"
	"testing"
)

var databaseTest, databaseDefaultMysql string

func init() {
	databaseTest = "test_kulit"
	databaseDefaultMysql = "mysql"
}

func TestDatabaseMysql(t *testing.T) {
	t.Run("Testing Koneksi Mysql", func(t *testing.T) {
		db, err := lib.ConnectMysql(databaseDefaultMysql)

		defer db.Close()

		if err != nil {
			t.Fatal(err)
		}
	})

	t.Run("Testing Drop", func(t *testing.T) {
		db, err := lib.ConnectMysql(databaseDefaultMysql)
		defer db.Close()
		if err != nil {
			t.Fatal(err)
		}
		err = lib.DropDB(db, databaseTest)
		if err != nil {
			t.Fatal(err)
		}
	})

	t.Run("Testing Create DB", func(t *testing.T) {
		db, err := lib.ConnectMysql(databaseDefaultMysql)
		defer db.Close()

		if err != nil {
			t.Fatal(err)
		}

		err = lib.CreateDB(db, databaseTest)
		if err != nil {
			t.Fatal(err)
		}
	})

	t.Run("Testing Create Table Mysql", func(t *testing.T) {
		db, err := lib.ConnectMysql(databaseTest)
		defer db.Close()

		if err != nil {
			t.Fatal(err)
		}

		err = lib.CreateTable(db, model.TableGejalasakit)
		if err != nil {
			t.Fatal(err)
		}

		err = lib.CreateTable(db, model.TablePenyakit)
		if err != nil {
			t.Fatal(err)
		}

		err = lib.CreateTable(db, model.TableRiwayat)
		if err != nil {
			t.Fatal(err)
		}

		err = lib.CreateTable(db, model.TableUsertb)
		if err != nil {
			t.Fatal(err)
		}

		err = lib.CreateTable(db, model.TableInfo)
		if err != nil {
			t.Fatal(err)
		}

		err = lib.CreateTable(db, model.TableHasildetail)
		if err != nil {
			t.Fatal(err)
		}

		err = lib.CreateTable(db, model.TableHasil)
		if err != nil {
			t.Fatal(err)
		}
	})
}
