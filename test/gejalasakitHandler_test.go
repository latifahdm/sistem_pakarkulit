package test

import (
	"bytes"
	"encoding/json"
	"fmt"
	"kulit/crud/handler"
	"kulit/crud/lib"
	"kulit/crud/model"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestGejalasakitHandler(t *testing.T) {
	t.Run("Testing Insert", func(t *testing.T) {
		db, err := lib.ConnectMysql(databaseTest)
		defer db.Close()
		if err != nil {
			t.Fatal(err)
		}
		data := []model.Gejalasakit{
			{Gejala_penyakit: "Demam"},
			{Gejala_penyakit: "Kulit Merah"},
			{Gejala_penyakit: "Ruam"},
		}

		for _, val := range data {
			jsonData, err := json.Marshal(val)
			if err != nil {
				t.Fatal(err)
			}
			b := bytes.NewBuffer(jsonData)
			r := httptest.NewRequest(http.MethodPost, "/api/gejalasakit", b)
			w := httptest.NewRecorder()
			handler.RegistDB(db)
			handler.API(w, r)

			status := w.Code
			if status != http.StatusOK {
				t.Fatalf("get : %v want : %v status : %v", status, http.StatusOK, w.Body)
			}
		}
	})

	t.Run("Testing Update", func(t *testing.T) {
		db, err := lib.ConnectMysql(databaseTest)
		defer db.Close()
		if err != nil {
			t.Fatal(err)
		}
		data := model.Gejalasakit{
			Gejala_penyakit: "bernanah",
		}
		jsonData, err := json.Marshal(data)
		if err != nil {
			t.Fatal(err)
		}
		b := bytes.NewBuffer(jsonData)
		r := httptest.NewRequest(http.MethodPost, "/api/gejalasakit/2", b)
		w := httptest.NewRecorder()
		handler.RegistDB(db)
		handler.API(w, r)

		status := w.Code
		if status != http.StatusOK {
			t.Fatalf("get : %v want : %v status : %v", status, http.StatusOK, w.Body)
		}
	})

	t.Run("Testing Delete", func(t *testing.T) {
		db, err := lib.ConnectMysql(databaseTest)
		defer db.Close()
		if err != nil {
			t.Fatal(err)
		}
		r := httptest.NewRequest(http.MethodDelete, "/api/gejalasakit/1", nil)
		w := httptest.NewRecorder()
		handler.RegistDB(db)
		handler.API(w, r)

		status := w.Code
		if status != http.StatusOK {
			t.Fatalf("get : %v want : %v status : %v", status, http.StatusOK, w.Body)
		}
	})

	t.Run("Testing Gets", func(t *testing.T) {
		db, err := lib.ConnectMysql(databaseTest)
		defer db.Close()
		if err != nil {
			t.Fatal(err)
		}
		r := httptest.NewRequest(http.MethodGet, "/api/gejalasakit", nil)
		w := httptest.NewRecorder()
		handler.RegistDB(db)
		handler.API(w, r)

		status := w.Code
		if status != http.StatusOK {
			t.Fatalf("get : %v want : %v status : %v", status, http.StatusOK, w.Body)
		}

		var jsonData = []byte(w.Body.String())
		var gejalasakit []model.Gejalasakit

		err = json.Unmarshal(jsonData, &gejalasakit)
		if err != nil {
			t.Fatal(err)
		}
		fmt.Println(gejalasakit)
	})

	//getsWithParams
	t.Run("Testing GetsWithParams", func(t *testing.T) {
		db, err := lib.ConnectMysql(databaseTest)
		defer db.Close()
		if err != nil {
			t.Fatal(err)
		}
		r := httptest.NewRequest(http.MethodGet, "/api/gejalasakit?params=id_gejala%2Clike%2C%251%25", nil)
		w := httptest.NewRecorder()
		handler.RegistDB(db)
		handler.API(w, r)

		status := w.Code
		if status != http.StatusOK {
			t.Fatalf("get : %v want : %v status : %v", status, http.StatusOK, w.Body)
		}

		var jsonData = []byte(w.Body.String())
		var gejalasakit []model.Gejalasakit

		err = json.Unmarshal(jsonData, &gejalasakit)
		if err != nil {
			t.Fatal(err)
		}
		fmt.Println(gejalasakit)
	})

	t.Run("Testing Get", func(t *testing.T) {
		db, err := lib.ConnectMysql(databaseTest)
		defer db.Close()
		if err != nil {
			t.Fatal(err)
		}
		r := httptest.NewRequest(http.MethodGet, "/api/gejalasakit/2", nil)
		w := httptest.NewRecorder()
		handler.RegistDB(db)
		handler.API(w, r)

		status := w.Code
		if status != http.StatusOK {
			t.Fatalf("get : %v want : %v status : %v", status, http.StatusOK, w.Body)
		}

		var jsonData = []byte(w.Body.String())
		var gejalasakit model.Gejalasakit

		err = json.Unmarshal(jsonData, &gejalasakit)
		if err != nil {
			t.Fatal(err)
		}
		fmt.Println(gejalasakit)
	})
}
