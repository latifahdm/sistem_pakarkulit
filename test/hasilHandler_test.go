package test

import (
	"bytes"
	"encoding/json"
	"fmt"
	"kulit/crud/handler"
	"kulit/crud/lib"
	"kulit/crud/model"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestHasilHandler(t *testing.T) {
	t.Run("Testing Insert", func(t *testing.T) {
		db, err := lib.ConnectMysql(databaseTest)
		defer db.Close()
		if err != nil {
			t.Fatal(err)
		}
		data7 := []model.Hasil{
			{Id_riwayat: 4, Hasil_akhir: 90.9, Penyakit: "Kudis"},
			{Id_riwayat: 5, Hasil_akhir: 95.8, Penyakit: "Panu"},
			{Id_riwayat: 6, Hasil_akhir: 91.3, Penyakit: "Cacar Air"},
		}

		for _, val := range data7 {
			jsonData, err := json.Marshal(val)
			if err != nil {
				t.Fatal(err)
			}
			b := bytes.NewBuffer(jsonData)
			r := httptest.NewRequest(http.MethodPost, "/api/hasil", b)
			w := httptest.NewRecorder()
			handler.RegistDB(db)
			handler.API(w, r)

			status := w.Code
			if status != http.StatusOK {
				t.Fatalf("get : %v want : %v status : %v", status, http.StatusOK, w.Body)
			}
		}
	})

	t.Run("Testing Update", func(t *testing.T) {
		db, err := lib.ConnectMysql(databaseTest)
		defer db.Close()
		if err != nil {
			t.Fatal(err)
		}
		data7 := model.Hasil{
			Hasil_akhir: 90.7,
		}
		jsonData, err := json.Marshal(data7)
		if err != nil {
			t.Fatal(err)
		}
		b := bytes.NewBuffer(jsonData)
		r := httptest.NewRequest(http.MethodPost, "/api/hasil/2", b)
		w := httptest.NewRecorder()
		handler.RegistDB(db)
		handler.API(w, r)

		status := w.Code
		if status != http.StatusOK {
			t.Fatalf("get : %v want : %v status : %v", status, http.StatusOK, w.Body)
		}
	})

	t.Run("Testing Delete", func(t *testing.T) {
		db, err := lib.ConnectMysql(databaseTest)
		defer db.Close()
		if err != nil {
			t.Fatal(err)
		}
		r := httptest.NewRequest(http.MethodDelete, "/api/hasil/1", nil)
		w := httptest.NewRecorder()
		handler.RegistDB(db)
		handler.API(w, r)

		status := w.Code
		if status != http.StatusOK {
			t.Fatalf("get : %v want : %v status : %v", status, http.StatusOK, w.Body)
		}
	})

	t.Run("Testing Gets", func(t *testing.T) {
		db, err := lib.ConnectMysql(databaseTest)
		defer db.Close()
		if err != nil {
			t.Fatal(err)
		}
		r := httptest.NewRequest(http.MethodGet, "/api/hasil", nil)
		w := httptest.NewRecorder()
		handler.RegistDB(db)
		handler.API(w, r)

		status := w.Code
		if status != http.StatusOK {
			t.Fatalf("get : %v want : %v status : %v", status, http.StatusOK, w.Body)
		}

		var jsonData = []byte(w.Body.String())
		var hasil []model.Hasil

		err = json.Unmarshal(jsonData, &hasil)
		if err != nil {
			t.Fatal(err)
		}
		fmt.Println(hasil)
	})

	//getsWithParams
	t.Run("Testing GetsWithParams", func(t *testing.T) {
		db, err := lib.ConnectMysql(databaseTest)
		defer db.Close()
		if err != nil {
			t.Fatal(err)
		}
		r := httptest.NewRequest(http.MethodGet, "/api/hasil?params=id_hasil%2Clike%2C%251%25", nil)
		w := httptest.NewRecorder()
		handler.RegistDB(db)
		handler.API(w, r)

		status := w.Code
		if status != http.StatusOK {
			t.Fatalf("get : %v want : %v status : %v", status, http.StatusOK, w.Body)
		}

		var jsonData = []byte(w.Body.String())
		var hasil []model.Hasil

		err = json.Unmarshal(jsonData, &hasil)
		if err != nil {
			t.Fatal(err)
		}
		fmt.Println(hasil)
	})

	t.Run("Testing Get", func(t *testing.T) {
		db, err := lib.ConnectMysql(databaseTest)
		defer db.Close()
		if err != nil {
			t.Fatal(err)
		}
		r := httptest.NewRequest(http.MethodGet, "/api/hasil/3", nil)
		w := httptest.NewRecorder()
		handler.RegistDB(db)
		handler.API(w, r)

		status := w.Code
		if status != http.StatusOK {
			t.Fatalf("get : %v want : %v status : %v", status, http.StatusOK, w.Body)
		}

		var jsonData = []byte(w.Body.String())
		var hasil model.Hasil

		err = json.Unmarshal(jsonData, &hasil)
		if err != nil {
			t.Fatal(err)
		}
		fmt.Println(hasil)
	})
}
