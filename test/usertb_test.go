package test

import (
	"fmt"
	"kulit/crud/lib"
	"kulit/crud/model"
	"testing"
)

var data4 = []*model.Usertb{
	&model.Usertb{Username: "latifahdm"},
	&model.Usertb{Username: "ohsehun"},
}

func TestUsertb(t *testing.T) {
	t.Run("Test Insert", func(t *testing.T) {
		db, err := lib.ConnectMysql(databaseTest)
		defer db.Close()
		if err != nil {
			t.Fatal(err)
		}

		for _, val := range data4 {
			err = val.Insert(db)
			if err != nil {
				t.Fatal(err)
			}
		}
	})

	t.Run("Test Update", func(t *testing.T) {
		db, err := lib.ConnectMysql(databaseTest)
		defer db.Close()
		if err != nil {
			t.Fatal(err)
		}

		dataUpdate := map[string]interface{}{
			"Nama": "Latifah",
		}

		err = data4[1].Update(db, dataUpdate)
		if err != nil {
			t.Fatal(err)
		}
	})

	t.Run("Test Delete", func(t *testing.T) {
		db, err := lib.ConnectMysql(databaseTest)
		defer db.Close()
		if err != nil {
			t.Fatal(err)
		}
		data4 := model.Usertb{Id_user: 2}
		err = data4.Delete(db)
		if err != nil {
			t.Fatal(err)
		}
	})

	t.Run("Test Get", func(t *testing.T) {
		db, err := lib.ConnectMysql(databaseTest)
		defer db.Close()
		if err != nil {
			t.Fatal(err)
		}
		data4 := model.Usertb{Id_user: 3}
		err = data4.Delete(db)
		if err != nil {
			t.Fatal(err)
		}
		fmt.Println(data4)
	})

	t.Run("Test Gets", func(t *testing.T) {
		db, err := lib.ConnectMysql(databaseTest)
		defer db.Close()
		if err != nil {
			t.Fatal(err)
		}
		data4, err := model.GetUsertb(db)
		if err != nil {
			t.Fatal(err)
		}
		for _, val := range data4 {
			fmt.Println(*val)
		}
	})
}
