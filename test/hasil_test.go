package test

import (
	"fmt"
	"kulit/crud/lib"
	"kulit/crud/model"
	"testing"
)

var data7 = []*model.Hasil{
	&model.Hasil{Id_riwayat: 1, Hasil_akhir: 98.7, Penyakit: "Kudis"},
	&model.Hasil{Id_riwayat: 2, Hasil_akhir: 97.8, Penyakit: "Panu"},
	&model.Hasil{Id_riwayat: 3, Hasil_akhir: 96.8, Penyakit: "Cacar Air"},
}

func TestHasil(t *testing.T) {
	t.Run("Test Insert", func(t *testing.T) {
		db, err := lib.ConnectMysql(databaseTest)
		defer db.Close()
		if err != nil {
			t.Fatal(err)
		}

		for _, val := range data7 {
			err = val.Insert(db)
			if err != nil {
				t.Fatal(err)
			}
		}
	})

	t.Run("Test Update", func(t *testing.T) {
		db, err := lib.ConnectMysql(databaseTest)
		defer db.Close()
		if err != nil {
			t.Fatal(err)
		}

		dataUpdate := map[string]interface{}{
			"penyakit": "Bisul",
		}

		err = data7[1].Update(db, dataUpdate)
		if err != nil {
			t.Fatal(err)
		}
	})

	t.Run("Test Delete", func(t *testing.T) {
		db, err := lib.ConnectMysql(databaseTest)
		defer db.Close()
		if err != nil {
			t.Fatal(err)
		}
		data7 := model.Hasil{Id_hasil: 2}
		err = data7.Delete(db)
		if err != nil {
			t.Fatal(err)
		}
	})

	t.Run("Test Get", func(t *testing.T) {
		db, err := lib.ConnectMysql(databaseTest)
		defer db.Close()
		if err != nil {
			t.Fatal(err)
		}
		data7 := model.Hasil{Id_hasil: 3}
		err = data7.Delete(db)
		if err != nil {
			t.Fatal(err)
		}
		fmt.Println(data7)
	})

	t.Run("Test Gets", func(t *testing.T) {
		db, err := lib.ConnectMysql(databaseTest)
		defer db.Close()
		if err != nil {
			t.Fatal(err)
		}
		data7, err := model.GetHasil(db)
		if err != nil {
			t.Fatal(err)
		}
		for _, val := range data7 {
			fmt.Println(*val)
		}
	})
}
