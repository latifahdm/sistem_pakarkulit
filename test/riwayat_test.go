package test

import (
	"fmt"
	"kulit/crud/lib"
	"kulit/crud/model"
	"testing"
)

var data3 = []*model.Riwayat{
	&model.Riwayat{Id_riwayat: 2, Id_user: 2, Penyakit: "Panu"},
	&model.Riwayat{Id_riwayat: 3, Id_user: 4, Penyakit: "cacar Air"},
}

func TestRiwayat(t *testing.T) {
	t.Run("Test Insert", func(t *testing.T) {
		db, err := lib.ConnectMysql(databaseTest)
		defer db.Close()
		if err != nil {
			t.Fatal(err)
		}

		for _, val := range data3 {
			err = val.Insert(db)
			if err != nil {
				t.Fatal(err)
			}
		}
	})

	t.Run("Test Update", func(t *testing.T) {
		db, err := lib.ConnectMysql(databaseTest)
		defer db.Close()
		if err != nil {
			t.Fatal(err)
		}

		dataUpdate := map[string]interface{}{
			"Penyakit": "Kudis",
		}

		err = data3[1].Update(db, dataUpdate)
		if err != nil {
			t.Fatal(err)
		}
	})

	t.Run("Test Delete", func(t *testing.T) {
		db, err := lib.ConnectMysql(databaseTest)
		defer db.Close()
		if err != nil {
			t.Fatal(err)
		}
		data3 := model.Riwayat{Id_riwayat: 2}
		err = data3.Delete(db)
		if err != nil {
			t.Fatal(err)
		}
	})

	t.Run("Test Get", func(t *testing.T) {
		db, err := lib.ConnectMysql(databaseTest)
		defer db.Close()
		if err != nil {
			t.Fatal(err)
		}
		data3 := model.Riwayat{Id_riwayat: 3}
		err = data3.Delete(db)
		if err != nil {
			t.Fatal(err)
		}
		fmt.Println(data3)
	})

	t.Run("Test Gets", func(t *testing.T) {
		db, err := lib.ConnectMysql(databaseTest)
		defer db.Close()
		if err != nil {
			t.Fatal(err)
		}
		data3, err := model.GetRiwayat(db)
		if err != nil {
			t.Fatal(err)
		}
		for _, val := range data3 {
			fmt.Println(*val)
		}
	})
}
