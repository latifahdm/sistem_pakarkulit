package test

import (
	"fmt"
	"kulit/crud/model"
	"testing"
)

var hit_test = []*model.Gejalasakit{
	&model.Gejalasakit{Id_penyakit: 1, Gejala_penyakit: "Berbentuk benjolan keras dan berisi nanah", Cf_pakar: 0.8},
	&model.Gejalasakit{Id_penyakit: 1, Gejala_penyakit: "Berwarna kemerahan dan meradang", Cf_pakar: 0.7},
	&model.Gejalasakit{Id_penyakit: 1, Gejala_penyakit: "Kelainan kulit yang letaknya seperti ketiak,bokong, leher dada dan paha ", Cf_pakar: 0.6},
	&model.Gejalasakit{Id_penyakit: 2, Gejala_penyakit: "Dapat berupa pustule (bintul berisi nanah) ", Cf_pakar: 0.8},
	&model.Gejalasakit{Id_penyakit: 2, Gejala_penyakit: "Gatal", Cf_pakar: 0.7},
	&model.Gejalasakit{Id_penyakit: 2, Gejala_penyakit: "Terdapat pada kedua sisi tubuh ", Cf_pakar: 0.6},
	&model.Gejalasakit{Id_penyakit: 2, Gejala_penyakit: "Kelainan kulit yang terletak terutama diantara jari-jari tangan dan kaki, disekeliling pergelangan tangan, siku", Cf_pakar: 0.6},
	&model.Gejalasakit{Id_penyakit: 3, Gejala_penyakit: "Bersisik dan halus, terkadang gatal jika berkeringat atau cuaca panas", Cf_pakar: 0.6},
	&model.Gejalasakit{Id_penyakit: 3, Gejala_penyakit: "Kelainan kulit berupa macula (datar)", Cf_pakar: 0.7},
	&model.Gejalasakit{Id_penyakit: 3, Gejala_penyakit: "Berwarna putih/pucat dibanding kulit aslinya", Cf_pakar: 0.8},
	&model.Gejalasakit{Id_penyakit: 3, Gejala_penyakit: "Bergerombol membentuk pulau-pulau", Cf_pakar: 0.7},
	&model.Gejalasakit{Id_penyakit: 3, Gejala_penyakit: "Mempunyai batas yang tegas antara kulit yang sakit dan yang sehat", Cf_pakar: 0.7},
	&model.Gejalasakit{Id_penyakit: 3, Gejala_penyakit: "Kering", Cf_pakar: 0.6},
	&model.Gejalasakit{Id_penyakit: 4, Gejala_penyakit: "Kering Bersisik", Cf_pakar: 0.6},
	&model.Gejalasakit{Id_penyakit: 4, Gejala_penyakit: "Berbentuk bulat tetapi bisa juga berbentuk lengkung", Cf_pakar: 0.7},
	&model.Gejalasakit{Id_penyakit: 4, Gejala_penyakit: "Berwarna abu-abu pada kulit kepala bila menginfeksi kulit kepala", Cf_pakar: 0.8},
	&model.Gejalasakit{Id_penyakit: 4, Gejala_penyakit: "Bila menginfeksi kepala bisa menimbulkan kerontokan rambut sampai meninggalkan pitak", Cf_pakar: 0.7},
	&model.Gejalasakit{Id_penyakit: 5, Gejala_penyakit: "Kelainan kulit berupa bintul berisi cairan jernih", Cf_pakar: 0.8},
	&model.Gejalasakit{Id_penyakit: 5, Gejala_penyakit: "Kelainan kulit berupa bintul berisi nanah", Cf_pakar: 0.5},
	&model.Gejalasakit{Id_penyakit: 5, Gejala_penyakit: "Basah", Cf_pakar: 0.7},
	&model.Gejalasakit{Id_penyakit: 5, Gejala_penyakit: "Di sertai demam , lelah dan tak enak badan", Cf_pakar: 0.7},
	&model.Gejalasakit{Id_penyakit: 5, Gejala_penyakit: "Kelainan kulit yang letak ruam mula-mula timbul pada badan lalu menjalar ke muka, lengan dan kaki", Cf_pakar: 0.7},
	&model.Gejalasakit{Id_penyakit: 6, Gejala_penyakit: "Kelainan kulit berupa bintul", Cf_pakar: 0.7},
	&model.Gejalasakit{Id_penyakit: 6, Gejala_penyakit: "Memiliki permukaan yang kasar, bentuknya bundar tidak beraturan, berwarna keabuan,kuning atau coklat, biasanya memiliki garis tengah kurang dari 1 cm", Cf_pakar: 0.8},
	&model.Gejalasakit{Id_penyakit: 6, Gejala_penyakit: "Kelainan kulit biasanya terdapat di muka, badan, tangan atau kaki", Cf_pakar: 0.7},
}

var hit_test2 = []*model.Hasildetail{
	&model.Hasildetail{Id_riwayat: 2, Id_penyakit: 1, Nama_gejala: "Berbentuk benjolan keras dan berisi nanah", Cf_pakar: 0.8},
	&model.Hasildetail{Id_riwayat: 3, Id_penyakit: 1, Nama_gejala: "Berwarna kemerahan dan meradang", Cf_pakar: 0.7},
	&model.Hasildetail{Id_riwayat: 4, Id_penyakit: 6, Nama_gejala: "Kelainan kulit biasanya terdapat di muka, badan, tangan atau kaki", Cf_pakar: 0.7},
	&model.Hasildetail{Id_riwayat: 5, Id_penyakit: 5, Nama_gejala: "Kelainan kulit berupa bintul berisi cairan jernih ", Cf_pakar: 0.8},
}

func TestHit(t *testing.T) {

	// var hasil int
	db, err := lib.ConnectMysql(databaseTest)
	defer db.Close()
	if err != nil {
		t.Fatal(err)
	}
	hasil, err := model.HitHasildetail(db, hit_test2)

	fmt.Println(hasil)
}
