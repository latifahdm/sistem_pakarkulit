package test

import (
	"bytes"
	"encoding/json"
	"fmt"
	"kulit/crud/handler"
	"kulit/crud/lib"
	"kulit/crud/model"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestPenyakitHandler(t *testing.T) {
	t.Run("Testing Insert", func(t *testing.T) {
		db, err := lib.ConnectMysql(databaseTest)
		defer db.Close()
		if err != nil {
			t.Fatal(err)
		}
		data2 := []model.Penyakit{
			{Jenis_penyakit: "Kutil"},
			{Jenis_penyakit: "Selulitis"},
			{Jenis_penyakit: "Cacar"},
		}

		for _, val := range data2 {
			jsonData, err := json.Marshal(val)
			if err != nil {
				t.Fatal(err)
			}
			b := bytes.NewBuffer(jsonData)
			r := httptest.NewRequest(http.MethodPost, "/api/penyakit", b)
			w := httptest.NewRecorder()
			handler.RegistDB(db)
			handler.API(w, r)

			status := w.Code
			if status != http.StatusOK {
				t.Fatalf("get : %v want : %v status : %v", status, http.StatusOK, w.Body)
			}
		}
	})

	t.Run("Testing Update", func(t *testing.T) {
		db, err := lib.ConnectMysql(databaseTest)
		defer db.Close()
		if err != nil {
			t.Fatal(err)
		}
		data2 := model.Penyakit{
			Jenis_penyakit: "jerawat",
		}
		jsonData, err := json.Marshal(data2)
		if err != nil {
			t.Fatal(err)
		}
		b := bytes.NewBuffer(jsonData)
		r := httptest.NewRequest(http.MethodPost, "/api/penyakit/2", b)
		w := httptest.NewRecorder()
		handler.RegistDB(db)
		handler.API(w, r)

		status := w.Code
		if status != http.StatusOK {
			t.Fatalf("get : %v want : %v status : %v", status, http.StatusOK, w.Body)
		}
	})

	t.Run("Testing Delete", func(t *testing.T) {
		db, err := lib.ConnectMysql(databaseTest)
		defer db.Close()
		if err != nil {
			t.Fatal(err)
		}
		r := httptest.NewRequest(http.MethodDelete, "/api/penyakit/1", nil)
		w := httptest.NewRecorder()
		handler.RegistDB(db)
		handler.API(w, r)

		status := w.Code
		if status != http.StatusOK {
			t.Fatalf("get : %v want : %v status : %v", status, http.StatusOK, w.Body)
		}
	})

	t.Run("Testing Gets", func(t *testing.T) {
		db, err := lib.ConnectMysql(databaseTest)
		defer db.Close()
		if err != nil {
			t.Fatal(err)
		}
		r := httptest.NewRequest(http.MethodGet, "/api/penyakit", nil)
		w := httptest.NewRecorder()
		handler.RegistDB(db)
		handler.API(w, r)

		status := w.Code
		if status != http.StatusOK {
			t.Fatalf("get : %v want : %v status : %v", status, http.StatusOK, w.Body)
		}

		var jsonData = []byte(w.Body.String())
		var penyakit []model.Penyakit

		err = json.Unmarshal(jsonData, &penyakit)
		if err != nil {
			t.Fatal(err)
		}
		fmt.Println(penyakit)
	})

	//getsWithParams
	t.Run("Testing GetsWithParams", func(t *testing.T) {
		db, err := lib.ConnectMysql(databaseTest)
		defer db.Close()
		if err != nil {
			t.Fatal(err)
		}
		r := httptest.NewRequest(http.MethodGet, "/api/penyakit?params=id_penyakit%2Clike%2C%251%25", nil)
		w := httptest.NewRecorder()
		handler.RegistDB(db)
		handler.API(w, r)

		status := w.Code
		if status != http.StatusOK {
			t.Fatalf("get : %v want : %v status : %v", status, http.StatusOK, w.Body)
		}

		var jsonData = []byte(w.Body.String())
		var penyakit []model.Penyakit

		err = json.Unmarshal(jsonData, &penyakit)
		if err != nil {
			t.Fatal(err)
		}
		fmt.Println(penyakit)
	})

	t.Run("Testing Get", func(t *testing.T) {
		db, err := lib.ConnectMysql(databaseTest)
		defer db.Close()
		if err != nil {
			t.Fatal(err)
		}
		r := httptest.NewRequest(http.MethodGet, "/api/penyakit/3", nil)
		w := httptest.NewRecorder()
		handler.RegistDB(db)
		handler.API(w, r)

		status := w.Code
		if status != http.StatusOK {
			t.Fatalf("get : %v want : %v status : %v", status, http.StatusOK, w.Body)
		}

		var jsonData = []byte(w.Body.String())
		var penyakit model.Penyakit

		err = json.Unmarshal(jsonData, &penyakit)
		if err != nil {
			t.Fatal(err)
		}
		fmt.Println(penyakit)
	})
}
