package test

import (
	"fmt"
	"kulit/crud/lib"
	"kulit/crud/model"
	"testing"
)

var data2 = []*model.Penyakit{
	&model.Penyakit{Jenis_penyakit: "Selulitis"},
	&model.Penyakit{Jenis_penyakit: "Impetigo"},
}

func TestPenyakit(t *testing.T) {
	t.Run("Test Insert", func(t *testing.T) {
		db, err := lib.ConnectMysql(databaseTest)
		defer db.Close()
		if err != nil {
			t.Fatal(err)
		}

		for _, val := range data2 {
			err = val.Insert(db)
			if err != nil {
				t.Fatal(err)
			}
		}
	})

	t.Run("Test Update", func(t *testing.T) {
		db, err := lib.ConnectMysql(databaseTest)
		defer db.Close()
		if err != nil {
			t.Fatal(err)
		}

		dataUpdate := map[string]interface{}{
			"Jenis_penyakit": "Cacar",
		}

		err = data2[1].Update(db, dataUpdate)
		if err != nil {
			t.Fatal(err)
		}
	})

	t.Run("Test Delete", func(t *testing.T) {
		db, err := lib.ConnectMysql(databaseTest)
		defer db.Close()
		if err != nil {
			t.Fatal(err)
		}
		data2 := model.Penyakit{Id_penyakit: 2}
		err = data2.Delete(db)
		if err != nil {
			t.Fatal(err)
		}
	})

	t.Run("Test Get", func(t *testing.T) {
		db, err := lib.ConnectMysql(databaseTest)
		defer db.Close()
		if err != nil {
			t.Fatal(err)
		}
		data2 := model.Penyakit{Id_penyakit: 3}
		err = data2.Delete(db)
		if err != nil {
			t.Fatal(err)
		}
		fmt.Println(data2)
	})

	t.Run("Test Gets", func(t *testing.T) {
		db, err := lib.ConnectMysql(databaseTest)
		defer db.Close()
		if err != nil {
			t.Fatal(err)
		}
		data2, err := model.GetPenyakit(db)
		if err != nil {
			t.Fatal(err)
		}
		for _, val := range data2 {
			fmt.Println(*val)
		}
	})
}
