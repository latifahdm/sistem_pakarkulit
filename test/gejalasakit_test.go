package test

import (
	"fmt"
	"kulit/crud/lib"
	"kulit/crud/model"
	"testing"
)

var data = []*model.Gejalasakit{
	&model.Gejalasakit{Gejala_penyakit: "Demam"},
	&model.Gejalasakit{Gejala_penyakit: "Kulit Merah"},
}

func TestGejalasakit(t *testing.T) {
	t.Run("Test Insert", func(t *testing.T) {
		db, err := lib.ConnectMysql(databaseTest)
		defer db.Close()
		if err != nil {
			t.Fatal(err)
		}

		for _, val := range data {
			err = val.Insert(db)
			if err != nil {
				t.Fatal(err)
			}
		}
	})

	t.Run("Test Update", func(t *testing.T) {
		db, err := lib.ConnectMysql(databaseTest)
		defer db.Close()
		if err != nil {
			t.Fatal(err)
		}

		dataUpdate := map[string]interface{}{
			"Gejala_penyakit": "Gatal-gatal",
		}

		err = data[1].Update(db, dataUpdate)
		if err != nil {
			t.Fatal(err)
		}
	})

	t.Run("Test Delete", func(t *testing.T) {
		db, err := lib.ConnectMysql(databaseTest)
		defer db.Close()
		if err != nil {
			t.Fatal(err)
		}
		data := model.Gejalasakit{Id_gejala: 2}
		err = data.Delete(db)
		if err != nil {
			t.Fatal(err)
		}
	})

	t.Run("Test Get", func(t *testing.T) {
		db, err := lib.ConnectMysql(databaseTest)
		defer db.Close()
		if err != nil {
			t.Fatal(err)
		}
		data := model.Gejalasakit{Id_gejala: 3}
		err = data.Delete(db)
		if err != nil {
			t.Fatal(err)
		}
		fmt.Println(data)
	})

	t.Run("Test Gets", func(t *testing.T) {
		db, err := lib.ConnectMysql(databaseTest)
		defer db.Close()
		if err != nil {
			t.Fatal(err)
		}
		data, err := model.GetGejalasakit(db)
		if err != nil {
			t.Fatal(err)
		}
		for _, val := range data {
			fmt.Println(*val)
		}
	})
}
