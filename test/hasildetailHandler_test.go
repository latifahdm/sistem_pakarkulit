package test

import (
	"bytes"
	"encoding/json"
	"fmt"
	"kulit/crud/handler"
	"kulit/crud/lib"
	"kulit/crud/model"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestHasildetailHandler(t *testing.T) {
	t.Run("Testing Insert", func(t *testing.T) {
		db, err := lib.ConnectMysql(databaseTest)
		defer db.Close()
		if err != nil {
			t.Fatal(err)
		}
		data6 := []*model.Hasildetail{
			&model.Hasildetail{Id_riwayat: 4, Id_penyakit: 1, Id_hasil: 4, Nama_gejala: "Gatal", Cf_pakar: 0.6},
			&model.Hasildetail{Id_riwayat: 5, Id_penyakit: 2, Id_hasil: 5, Nama_gejala: "Bintul", Cf_pakar: 0.7},
			&model.Hasildetail{Id_riwayat: 6, Id_penyakit: 3, Id_hasil: 6, Nama_gejala: "Datar", Cf_pakar: 0.5},
		}
		jsonData, err := json.Marshal(data6)
		if err != nil {
			t.Fatal(err)
		}
		// for _, val := range data6 {
		// 	jsonData, err := json.Marshal(val)
		// 	if err != nil {
		// 		t.Fatal(err)
		// 	}
		b := bytes.NewBuffer(jsonData)
		r := httptest.NewRequest(http.MethodPost, "/api/hasildetail?id_user=2", b)
		w := httptest.NewRecorder()
		handler.RegistDB(db)
		handler.API(w, r)

		status := w.Code
		if status != http.StatusOK {
			t.Fatalf("get : %v want : %v status : %v", status, http.StatusOK, w.Body)
		}
	})

	t.Run("Testing Update", func(t *testing.T) {
		db, err := lib.ConnectMysql(databaseTest)
		defer db.Close()
		if err != nil {
			t.Fatal(err)
		}
		data6 := model.Hasildetail{
			Nama_gejala: "Bintul berisi nanah",
		}
		jsonData, err := json.Marshal(data6)
		if err != nil {
			t.Fatal(err)
		}
		b := bytes.NewBuffer(jsonData)
		r := httptest.NewRequest(http.MethodPost, "/api/hasildetail/2", b)
		w := httptest.NewRecorder()
		handler.RegistDB(db)
		handler.API(w, r)

		status := w.Code
		if status != http.StatusOK {
			t.Fatalf("get : %v want : %v status : %v", status, http.StatusOK, w.Body)
		}
	})

	t.Run("Testing Delete", func(t *testing.T) {
		db, err := lib.ConnectMysql(databaseTest)
		defer db.Close()
		if err != nil {
			t.Fatal(err)
		}
		r := httptest.NewRequest(http.MethodDelete, "/api/hasildetail/1", nil)
		w := httptest.NewRecorder()
		handler.RegistDB(db)
		handler.API(w, r)

		status := w.Code
		if status != http.StatusOK {
			t.Fatalf("get : %v want : %v status : %v", status, http.StatusOK, w.Body)
		}
	})

	t.Run("Testing Gets", func(t *testing.T) {
		db, err := lib.ConnectMysql(databaseTest)
		defer db.Close()
		if err != nil {
			t.Fatal(err)
		}
		r := httptest.NewRequest(http.MethodGet, "/api/hasildetail", nil)
		w := httptest.NewRecorder()
		handler.RegistDB(db)
		handler.API(w, r)

		status := w.Code
		if status != http.StatusOK {
			t.Fatalf("get : %v want : %v status : %v", status, http.StatusOK, w.Body)
		}

		var jsonData = []byte(w.Body.String())
		var hasildetail []model.Hasildetail

		err = json.Unmarshal(jsonData, &hasildetail)
		if err != nil {
			t.Fatal(err)
		}
		fmt.Println(hasildetail)
	})

	//getsWithParams
	t.Run("Testing GetsWithParams", func(t *testing.T) {
		db, err := lib.ConnectMysql(databaseTest)
		defer db.Close()
		if err != nil {
			t.Fatal(err)
		}
		r := httptest.NewRequest(http.MethodGet, "/api/hasildetail?params=id_pil%2Clike%2C%251%25", nil)
		w := httptest.NewRecorder()
		handler.RegistDB(db)
		handler.API(w, r)

		status := w.Code
		if status != http.StatusOK {
			t.Fatalf("get : %v want : %v status : %v", status, http.StatusOK, w.Body)
		}

		var jsonData = []byte(w.Body.String())
		var hasildetail []model.Hasildetail

		err = json.Unmarshal(jsonData, &hasildetail)
		if err != nil {
			t.Fatal(err)
		}
		fmt.Println(hasildetail)
	})

	t.Run("Testing Get", func(t *testing.T) {
		db, err := lib.ConnectMysql(databaseTest)
		defer db.Close()
		if err != nil {
			t.Fatal(err)
		}
		r := httptest.NewRequest(http.MethodGet, "/api/hasildetail/3", nil)
		w := httptest.NewRecorder()
		handler.RegistDB(db)
		handler.API(w, r)

		status := w.Code
		if status != http.StatusOK {
			t.Fatalf("get : %v want : %v status : %v", status, http.StatusOK, w.Body)
		}

		var jsonData = []byte(w.Body.String())
		var hasildetail model.Hasildetail

		err = json.Unmarshal(jsonData, &hasildetail)
		if err != nil {
			t.Fatal(err)
		}
		fmt.Println(hasildetail)
	})
}
