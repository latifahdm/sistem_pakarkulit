package model

import (
	"database/sql"
	"fmt"
	"kulit/crud/lib"
	"strings"
)

type Hasil struct {
	Id_hasil    int     `json:"id_hasil"`
	Id_riwayat  int     `json:"id_riwayat"`
	Hasil_akhir float32 `json:"hasil_akhir"`
	Penyakit    string  `json:"penyakit"`
	Pilihan     []*Hasildetail
}

var TableHasil = lib.Table{
	Name: "Hasil",
	Field: []string{
		"id_hasil 				INT PRIMARY KEY AUTO_INCREMENT",
		"id_riwayat 			INT",
		"hasil_akhir    	FLOAT",
		"penyakit					VARCHAR(30)",
	},
}

func (m *Hasil) Insert(db *sql.DB) error {
	query := "INSERT INTO hasil (id_riwayat, hasil_akhir,penyakit) VALUES (?,?,?)"
	_, err := db.Exec(query, m.Id_riwayat, m.Hasil_akhir, m.Penyakit)
	if err != nil {
		return err
	}
	query = "SELECT MAX(id_hasil) from hasil"
	var id int
	err = db.QueryRow(query).Scan(&id)
	if err != nil {
		return err
	}
	m.Id_hasil = id
	return err
}

func (m *Hasil) Delete(db *sql.DB) error {
	query := "DELETE FROM hasil WHERE id_hasil = ?"
	_, err := db.Exec(query, m.Id_hasil)
	return err
}

func (m *Hasil) Update(db *sql.DB, data map[string]interface{}) error {
	var kolom = []string{}
	var args []interface{}
	for key, value := range data {
		if value == "" {
			continue
		}
		updateData := fmt.Sprintf("%v = ?", strings.ToLower(key))
		kolom = append(kolom, updateData)
		args = append(args, value)
	}
	dataUpdate := strings.Join(kolom, ",")
	query := fmt.Sprintf("UPDATE hasil SET %s WHERE id_hasil = '%d'", dataUpdate, m.Id_hasil)
	_, err := db.Exec(query, args...)
	fmt.Println(query)
	return err
}

func (m *Hasil) Get(db *sql.DB) error {
	query := "SELECT * FROM hasil WHERE id_hasil = ?"
	err := db.QueryRow(query, m.Id_hasil).Scan(&m.Id_hasil, &m.Id_riwayat, &m.Hasil_akhir, &m.Penyakit)
	return err
}

func GetHasil(db *sql.DB, params ...string) ([]*Hasil, error) {
	var kolom = []string{}
	var args []interface{}
	if len(params) != 0 {
		if params[0] != "" {
			dataParams := strings.Split(params[len(params)-1], ";")
			for _, v := range dataParams {
				temp := strings.Split(fmt.Sprintf("%s", v), ",")
				where := fmt.Sprintf("%s %s ?", strings.ToLower(temp[0]), temp[1])
				kolom = append(kolom, where)
				args = append(args, temp[2])
			}
		}
	}
	query := "SELECT * FROM hasil"

	dataKondisi := strings.Join(kolom, " AND ")
	if dataKondisi != "" {
		query += " WHERE " + dataKondisi
	}
	fmt.Println(query)
	fmt.Println(args)

	data, err := db.Query(query, args...)

	if err != nil {
		return nil, err
	}
	defer data.Close()
	var result []*Hasil
	for data.Next() {
		each := &Hasil{}
		err := data.Scan(&each.Id_hasil, &each.Id_riwayat, &each.Hasil_akhir, &each.Penyakit)
		if err != nil {
			return nil, err
		}
		result = append(result, each)
	}
	return result, nil
}
