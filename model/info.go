package model

import (
	"database/sql"
	"fmt"
	"kulit/crud/lib"
	"strings"
)

type Info struct {
	Id_info int    `json:"id_info"`
	Judul   string `json:"judul"`
	Isi     string `json:"isi"`
	Sumber  string `json:"sumber"`
}

var TableInfo = lib.Table{
	Name: "Info",
	Field: []string{
		"id_info 			INT PRIMARY KEY AUTO_INCREMENT",
		"judul 	      VARCHAR(40)",
		"isi 	        TEXT",
		"sumber 	    VARCHAR(30)",
	},
}

func (m *Info) Insert(db *sql.DB) error {
	query := "INSERT INTO Info (judul,isi,sumber) VALUES (?,?,?)"
	_, err := db.Exec(query, m.Judul, m.Isi, m.Sumber)
	return err
}

func (m *Info) Delete(db *sql.DB) error {
	query := "DELETE FROM Info WHERE id_info = ?"
	_, err := db.Exec(query, m.Id_info)
	return err
}

func (m *Info) Update(db *sql.DB, data map[string]interface{}) error {
	var kolom = []string{}
	var args []interface{}
	for key, value := range data {
		if value == "" {
			continue
		}
		updateData := fmt.Sprintf("%v = ?", strings.ToLower(key))
		kolom = append(kolom, updateData)
		args = append(args, value)
	}
	dataUpdate := strings.Join(kolom, ",")
	query := fmt.Sprintf("UPDATE Info SET %s WHERE id_info = '%d'", dataUpdate, m.Id_info)
	_, err := db.Exec(query, args...)
	fmt.Println(query)
	return err
}

func (m *Info) Get(db *sql.DB) error {
	query := "SELECT * FROM Info WHERE id_info = ?"
	err := db.QueryRow(query, m.Id_info).Scan(&m.Id_info, &m.Judul, &m.Isi, &m.Sumber)
	return err
}

func GetInfo(db *sql.DB, params ...string) ([]*Info, error) {
	var kolom = []string{}
	var args []interface{}
	if len(params) != 0 {
		if params[0] != "" {
			dataParams := strings.Split(params[len(params)-1], ";")
			for _, v := range dataParams {
				temp := strings.Split(fmt.Sprintf("%s", v), ",")
				where := fmt.Sprintf("%s %s ?", strings.ToLower(temp[0]), temp[1])
				kolom = append(kolom, where)
				args = append(args, temp[2])
			}
		}
	}
	query := "SELECT * FROM Info"

	dataKondisi := strings.Join(kolom, " AND ")
	if dataKondisi != "" {
		query += " WHERE " + dataKondisi
	}
	fmt.Println(query)
	fmt.Println(args)

	data, err := db.Query(query, args...)

	if err != nil {
		return nil, err
	}
	defer data.Close()
	var result []*Info
	for data.Next() {
		each := &Info{}
		err := data.Scan(&each.Id_info, &each.Judul, &each.Isi, &each.Sumber)
		if err != nil {
			return nil, err
		}
		result = append(result, each)
	}
	return result, nil
}
