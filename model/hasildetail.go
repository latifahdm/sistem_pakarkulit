package model

import (
	"database/sql"
	"fmt"
	"kulit/crud/lib"
	"strings"
)

type Hasildetail struct {
	Id_pil      int     `json:"id_pil"`
	Id_riwayat  int     `json:"id_riwayat"`
	Id_penyakit int     `json:"id_penyakit"`
	Id_hasil    int     `json:"id_hasil"`
	Nama_gejala string  `json:"nama_gejala"`
	Cf_pakar    float32 `json:"cf_pakar"`
}

var TableHasildetail = lib.Table{
	Name: "Hasildetail",
	Field: []string{
		"id_pil       INT PRIMARY KEY AUTO_INCREMENT",
		"id_riwayat 	INT",
		"id_penyakit	INT",
		"id_hasil	INT",
		"nama_gejala	TEXT",
		"cf_pakar 	  FLOAT",
	},
}

func (m *Hasildetail) Insert(db *sql.DB) error {
	query := "INSERT INTO hasildetail (id_riwayat,id_penyakit,id_hasil, nama_gejala,cf_pakar) VALUES (?,?,?,?,?)"
	_, err := db.Exec(query, m.Id_riwayat, m.Id_penyakit, m.Id_hasil, m.Nama_gejala, m.Cf_pakar)
	return err
}

func (m *Hasildetail) Delete(db *sql.DB) error {
	query := "DELETE FROM hasildetail WHERE id_pil = ?"
	_, err := db.Exec(query, m.Id_pil)
	return err
}

func (m *Hasildetail) Update(db *sql.DB, data map[string]interface{}) error {
	var kolom = []string{}
	var args []interface{}
	for key, value := range data {
		if value == "" {
			continue
		}
		updateData := fmt.Sprintf("%v = ?", strings.ToLower(key))
		kolom = append(kolom, updateData)
		args = append(args, value)
	}
	dataUpdate := strings.Join(kolom, ",")
	query := fmt.Sprintf("UPDATE hasildetail SET %s WHERE id_pil = '%d'", dataUpdate, m.Id_pil)
	_, err := db.Exec(query, args...)
	fmt.Println(query)
	return err
}

func (m *Hasildetail) Get(db *sql.DB) error {
	query := "SELECT * FROM hasildetail WHERE id_pil = ?"
	err := db.QueryRow(query, m.Id_pil).Scan(&m.Id_pil, &m.Id_riwayat, &m.Id_penyakit, &m.Id_hasil, &m.Nama_gejala, &m.Cf_pakar)
	return err
}

func GetHasildetail(db *sql.DB, params ...string) ([]*Hasildetail, error) {
	var kolom = []string{}
	var args []interface{}
	if len(params) != 0 {
		if params[0] != "" {
			dataParams := strings.Split(params[len(params)-1], ";")
			for _, v := range dataParams {
				temp := strings.Split(fmt.Sprintf("%s", v), ",")
				where := fmt.Sprintf("%s %s ?", strings.ToLower(temp[0]), temp[1])
				kolom = append(kolom, where)
				args = append(args, temp[2])
			}
		}
	}
	query := "SELECT * FROM hasildetail"

	dataKondisi := strings.Join(kolom, " AND ")
	if dataKondisi != "" {
		query += " WHERE " + dataKondisi
	}
	fmt.Println(query)
	fmt.Println(args)

	data, err := db.Query(query, args...)

	if err != nil {
		return nil, err
	}
	defer data.Close()
	var result []*Hasildetail
	for data.Next() {
		each := &Hasildetail{}
		err := data.Scan(&each.Id_pil, &each.Id_riwayat, &each.Id_penyakit, &each.Id_hasil, &each.Nama_gejala, &each.Cf_pakar)
		if err != nil {
			return nil, err
		}
		result = append(result, each)
	}
	return result, nil
}

func HitHasildetail(db *sql.DB, data []*Hasildetail) ([]Hasil, error) {
	// type hasilpenyakit struct {
	// 	id_penyakit int
	// 	hasil       float32
	// }
	type sortdata struct {
		id_penyakit int
		hasildetail []*Hasildetail
	}
	var x []sortdata
	for i := 0; i < len(data); i++ {
		var item sortdata
		if i == 0 {
			item.id_penyakit = data[i].Id_penyakit
			item.hasildetail = append(item.hasildetail, data[i])
		} else {
			cek := false
			idx := 0
			for j, p := range x {
				if data[i].Id_penyakit == p.id_penyakit {
					cek = true
					idx = j
					break
				}
			}
			if cek {
				x[idx].hasildetail = append(x[idx].hasildetail, data[i])
			} else {
				item.id_penyakit = data[i].Id_penyakit
				item.hasildetail = append(item.hasildetail, data[i])
			}
		}
		if item.id_penyakit != 0 {
			x = append(x, item)
		}
	}
	// fmt.Println("yformat")
	// fmt.Printf("Hasildetail : \n%s\n", data)
	// fmt.Printf("total data : %d\n", len(data))
	// fmt.Printf("Sorting : \n%s\n\n", x)
	for _, item := range x {
		fmt.Println(item.id_penyakit)
		for _, chield := range item.hasildetail {
			fmt.Println(chield)
		}
	}
	var result []Hasil
	for i, item := range x {
		fmt.Println(i)
		var hp Hasil
		penyakit := Penyakit{Id_penyakit: item.id_penyakit}
		if err := penyakit.Get(db); err != nil {
			return nil, err
		}
		hp.Penyakit = penyakit.Jenis_penyakit
		// hp.id_penyakit = item.id_penyakit
		index := 0
		len_data := 0
		var temp float32
		if len(item.hasildetail) == 1 {
			hp.Hasil_akhir = temp
			result = append(result, hp)
			continue
		} else if len(item.hasildetail) == 2 {
			len_data = 1
		} else {
			len_data = (len(item.hasildetail) - 1)
		}
		for i := 0; i < len_data; i++ {
			if i == 0 {
				temp = item.hasildetail[index].Cf_pakar + (item.hasildetail[index+1].Cf_pakar * (1 - item.hasildetail[index].Cf_pakar))
				index += 2
			} else {
				temp = item.hasildetail[index].Cf_pakar + (temp * (1 - item.hasildetail[index].Cf_pakar))
				index++
			}
		}

		fmt.Println(len_data)
		hp.Hasil_akhir = temp
		if i == len(x)-1 {
			hp.Pilihan = item.hasildetail
		}
		result = append(result, hp)
	}
	// fmt.Println("result")
	// fmt.Printf("%s\n", result)
	// resultPenyakit := result[0].id_penyakit
	// if len(result) > 1 {
	// 	temp := result[0].hasil
	// 	for i := 1; i < len(result); i++ {
	// 		if temp < result[i].hasil {
	// 			resultPenyakit = result[i].id_penyakit
	// 		}
	// 	}
	// }
	// for _, item := range result {
	// 	fmt.Println(item)
	// }
	// fmt.Printf("id_penyakit : %d", resultPenyakit)
	// dilakukan ketika item result>1 kalo ga keluar sebagai hasil//
	return result, nil
}

// func Hasil() {
// 	var pil []Hasildetail
// 	fmt.Println(HitHasildetail(pil))
// }
