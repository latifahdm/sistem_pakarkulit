package model

import (
	"database/sql"
	"fmt"
	"kulit/crud/lib"
	"strings"
)

type Penyakit struct {
	Id_penyakit    int    `json:"id_penyakit"`
	Jenis_penyakit string `json:"jenis_penyakit"`
}

var TablePenyakit = lib.Table{
	Name: "Penyakit",
	Field: []string{
		"id_penyakit 			INT PRIMARY KEY AUTO_INCREMENT",
		"jenis_penyakit 	VARCHAR(30)",
	},
}

func (m *Penyakit) Insert(db *sql.DB) error {
	query := "INSERT INTO penyakit (jenis_penyakit) VALUES (?)"
	_, err := db.Exec(query, m.Jenis_penyakit)
	return err
}

func (m *Penyakit) Delete(db *sql.DB) error {
	query := "DELETE FROM penyakit WHERE id_penyakit = ?"
	_, err := db.Exec(query, m.Id_penyakit)
	return err
}

func (m *Penyakit) Update(db *sql.DB, data map[string]interface{}) error {
	var kolom = []string{}
	var args []interface{}
	for key, value := range data {
		if value == "" {
			continue
		}
		updateData := fmt.Sprintf("%v = ?", strings.ToLower(key))
		kolom = append(kolom, updateData)
		args = append(args, value)
	}
	dataUpdate := strings.Join(kolom, ",")
	query := fmt.Sprintf("UPDATE penyakit SET %s WHERE id_penyakit = '%d'", dataUpdate, m.Id_penyakit)
	_, err := db.Exec(query, args...)
	fmt.Println(query)
	return err
}

func (m *Penyakit) Get(db *sql.DB) error {
	query := "SELECT * FROM penyakit WHERE id_penyakit = ?"
	err := db.QueryRow(query, m.Id_penyakit).Scan(&m.Id_penyakit, &m.Jenis_penyakit)
	return err
}

func GetPenyakit(db *sql.DB, params ...string) ([]*Penyakit, error) {
	var kolom = []string{}
	var args []interface{}
	if len(params) != 0 {
		if params[0] != "" {
			dataParams := strings.Split(params[len(params)-1], ";")
			for _, v := range dataParams {
				temp := strings.Split(fmt.Sprintf("%s", v), ",")
				where := fmt.Sprintf("%s %s ?", strings.ToLower(temp[0]), temp[1])
				kolom = append(kolom, where)
				args = append(args, temp[2])
			}
		}
	}
	query := "SELECT * FROM penyakit"

	dataKondisi := strings.Join(kolom, " AND ")
	if dataKondisi != "" {
		query += " WHERE " + dataKondisi
	}
	fmt.Println(query)
	fmt.Println(args)

	data, err := db.Query(query, args...)

	if err != nil {
		return nil, err
	}
	defer data.Close()
	var result []*Penyakit
	for data.Next() {
		each := &Penyakit{}
		err := data.Scan(&each.Id_penyakit, &each.Jenis_penyakit)
		if err != nil {
			return nil, err
		}
		result = append(result, each)
	}
	return result, nil
}
