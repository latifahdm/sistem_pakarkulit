package model

import (
	"database/sql"
	"fmt"
	"kulit/crud/lib"
	"strings"
	"time"
)

type Riwayat struct {
	Id_riwayat int       `json:"id_riwayat"`
	Tanggal    time.Time `json:"tanggal"`
	Penyakit   string    `json:"penyakit"`
	Id_user    int       `json:"id_user"`
	Usertb     Usertb    `json:"usertb"`
}

var TableRiwayat = lib.Table{
	Name: "Riwayat",
	Field: []string{
		"id_riwayat INT PRIMARY KEY AUTO_INCREMENT",
		"tanggal		DATE",
		"penyakit 	VARCHAR(50)",
		"id_user 		INT",
	},
}

func (m *Riwayat) Insert(db *sql.DB) error {
	query := "INSERT INTO riwayat (tanggal,penyakit) VALUES (?,?)"
	_, err := db.Exec(query, m.Tanggal, m.Penyakit)
	if err != nil {
		return err
	}
	query = "SELECT MAX(id_riwayat) from riwayat"
	var id int
	err = db.QueryRow(query).Scan(&id)
	if err != nil {
		return err
	}
	m.Id_riwayat = id
	return err
}

func (m *Riwayat) Delete(db *sql.DB) error {
	query := "DELETE FROM riwayat WHERE id_riwayat = ?"
	_, err := db.Exec(query, m.Id_riwayat)
	return err
}

func (m *Riwayat) Update(db *sql.DB, data map[string]interface{}) error {
	var kolom = []string{}
	var args []interface{}
	for key, value := range data {
		if value == "" {
			continue
		}
		updateData := fmt.Sprintf("%v = ?", strings.ToLower(key))
		kolom = append(kolom, updateData)
		args = append(args, value)
	}
	dataUpdate := strings.Join(kolom, ",")
	query := fmt.Sprintf("UPDATE riwayat SET %s WHERE id_riwayat = '%d'", dataUpdate, m.Id_riwayat)
	_, err := db.Exec(query, args...)
	fmt.Println(query)
	return err
}

func (m *Riwayat) Get(db *sql.DB) error {
	query := "SELECT * FROM riwayat INNER JOIN usertb ON riwayat.id_user = usertb.id_user WHERE riwayat.id_riwayat = ?"
	err := db.QueryRow(query, m.Id_riwayat).Scan(&m.Id_riwayat, &m.Tanggal, &m.Penyakit, &m.Id_user, &m.Usertb.Id_user, &m.Usertb.Username, &m.Usertb.Password, &m.Usertb.Nama, &m.Usertb.Email, &m.Usertb.T_lahir, &m.Usertb.J_kelamin, &m.Usertb.Level)
	return err
}

func GetRiwayat(db *sql.DB, params ...string) ([]*Riwayat, error) {
	var kolom = []string{}
	var args []interface{}
	if len(params) != 0 {
		if params[0] != "" {
			dataParams := strings.Split(params[len(params)-1], ";")
			for _, v := range dataParams {
				temp := strings.Split(fmt.Sprintf("%s", v), ",")
				where := fmt.Sprintf("%s %s ?", strings.ToLower(temp[0]), temp[1])
				kolom = append(kolom, where)
				args = append(args, temp[2])
			}
		}
	}
	query := "SELECT * FROM riwayat INNER JOIN usertb ON riwayat.id_user= usertb.id_user"

	dataKondisi := strings.Join(kolom, " AND ")
	if dataKondisi != "" {
		query += " WHERE " + dataKondisi
	}
	fmt.Println(query)
	fmt.Println(args)

	data, err := db.Query(query, args...)

	if err != nil {
		return nil, err
	}
	defer data.Close()
	var result []*Riwayat
	for data.Next() {
		each := &Riwayat{}
		err := data.Scan(&each.Id_riwayat, &each.Tanggal, &each.Penyakit, &each.Id_user,
			&each.Usertb.Id_user, &each.Usertb.Username, &each.Usertb.Password, &each.Usertb.Nama, &each.Usertb.Email, &each.Usertb.T_lahir, &each.Usertb.J_kelamin, &each.Usertb.Level)
		if err != nil {
			return nil, err
		}
		result = append(result, each)
	}
	return result, nil
}
