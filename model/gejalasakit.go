package model

import (
	"database/sql"
	"fmt"
	"kulit/crud/lib"
	"strings"
)

type Gejalasakit struct {
	Id_gejala       int     `json:"id_gejala"`
	Id_penyakit     int     `json:"id_penyakit"`
	Gejala_penyakit string  `json:"gejala_penyakit"`
	Cf_pakar        float32 `json:"cf_pakar"`
}

var TableGejalasakit = lib.Table{
	Name: "Gejalasakit",
	Field: []string{
		"id_gejala 				INT PRIMARY KEY AUTO_INCREMENT",
		"id_penyakit 			INT",
		"gejala_penyakit 	TEXT",
		"cf_pakar					FLOAT",
	},
}

func (m *Gejalasakit) Insert(db *sql.DB) error {
	query := "INSERT INTO gejalasakit (id_penyakit, gejala_penyakit,cf_pakar) VALUES (?,?,?)"
	_, err := db.Exec(query, m.Id_penyakit, m.Gejala_penyakit, m.Cf_pakar)
	return err
}

func (m *Gejalasakit) Delete(db *sql.DB) error {
	query := "DELETE FROM gejalasakit WHERE id_gejala = ?"
	_, err := db.Exec(query, m.Id_gejala)
	return err
}

func (m *Gejalasakit) Update(db *sql.DB, data map[string]interface{}) error {
	var kolom = []string{}
	var args []interface{}
	for key, value := range data {
		if value == "" {
			continue
		}
		updateData := fmt.Sprintf("%v = ?", strings.ToLower(key))
		kolom = append(kolom, updateData)
		args = append(args, value)
	}
	dataUpdate := strings.Join(kolom, ",")
	query := fmt.Sprintf("UPDATE gejalasakit SET %s WHERE id_gejala = '%d'", dataUpdate, m.Id_gejala)
	_, err := db.Exec(query, args...)
	fmt.Println(query)
	return err
}

func (m *Gejalasakit) Get(db *sql.DB) error {
	query := "SELECT * FROM gejalasakit WHERE id_gejala = ?"
	err := db.QueryRow(query, m.Id_gejala).Scan(&m.Id_gejala, &m.Id_penyakit, &m.Gejala_penyakit, &m.Cf_pakar)
	return err
}

func GetGejalasakit(db *sql.DB, params ...string) ([]*Gejalasakit, error) {
	var kolom = []string{}
	var args []interface{}
	if len(params) != 0 {
		if params[0] != "" {
			dataParams := strings.Split(params[len(params)-1], ";")
			for _, v := range dataParams {
				temp := strings.Split(fmt.Sprintf("%s", v), ",")
				where := fmt.Sprintf("%s %s ?", strings.ToLower(temp[0]), temp[1])
				kolom = append(kolom, where)
				args = append(args, temp[2])
			}
		}
	}
	query := "SELECT * FROM gejalasakit"

	dataKondisi := strings.Join(kolom, " AND ")
	if dataKondisi != "" {
		query += " WHERE " + dataKondisi
	}
	fmt.Println(query)
	fmt.Println(args)

	data, err := db.Query(query, args...)

	if err != nil {
		return nil, err
	}
	defer data.Close()
	var result []*Gejalasakit
	for data.Next() {
		each := &Gejalasakit{}
		err := data.Scan(&each.Id_gejala, &each.Id_penyakit, &each.Gejala_penyakit, &each.Cf_pakar)
		if err != nil {
			return nil, err
		}
		result = append(result, each)
	}
	return result, nil
}
