package model

import (
	"database/sql"
	"fmt"
	"kulit/crud/lib"
	"strings"
	"time"
)

type Usertb struct {
	Id_user   int       `json:"id_user"`
	Username  string    `json:"username"`
	Password  string    `json:"password"`
	Nama      string    `json:"nama"`
	Email     string    `json:"email"`
	T_lahir   time.Time `json:"t_lahir"`
	J_kelamin string    `json:"j_kelamin"`
	Level     string    `json:"level"`
}

var TableUsertb = lib.Table{
	Name: "Usertb",
	Field: []string{
		"id_user      INT					 PRIMARY KEY AUTO_INCREMENT",
		"username     VARCHAR(15)",
		"password     VARCHAR(30)",
		"nama         VARCHAR(50)",
		"email        VARCHAR(30)",
		"t_lahir      DATE",
		"j_kelamin    VARCHAR(10)",
		"level		    VARCHAR(30)",
	},
}

func (m *Usertb) Insert(db *sql.DB) error {
	query := "INSERT INTO usertb (username,password,nama,email,t_lahir,j_kelamin,level) VALUES (?,?,?,?,?,?,?)"
	_, err := db.Exec(query, m.Username, m.Password, m.Nama, m.Email, m.T_lahir, m.J_kelamin, m.Level)
	return err
}

func (m *Usertb) Delete(db *sql.DB) error {
	query := "DELETE FROM usertb WHERE id_user = ?"
	_, err := db.Exec(query, m.Id_user)
	return err
}

func (m *Usertb) Update(db *sql.DB, data map[string]interface{}) error {
	var kolom = []string{}
	var args []interface{}
	for key, value := range data {
		if value == "" {
			continue
		}
		updateData := fmt.Sprintf("%v = ?", strings.ToLower(key))
		kolom = append(kolom, updateData)
		args = append(args, value)
	}
	dataUpdate := strings.Join(kolom, ",")
	query := fmt.Sprintf("UPDATE usertb SET %s WHERE id_user = '%d'", dataUpdate, m.Id_user)
	_, err := db.Exec(query, args...)
	fmt.Println(query)
	return err
}

func (m *Usertb) Get(db *sql.DB) error {
	query := "SELECT * FROM usertb WHERE id_user = ?"
	err := db.QueryRow(query, m.Id_user).Scan(&m.Id_user, &m.Username, &m.Password, &m.Nama, &m.Email, &m.T_lahir, &m.J_kelamin, &m.Level)
	return err
}

func GetUsertb(db *sql.DB, params ...string) ([]*Usertb, error) {
	var kolom = []string{}
	var args []interface{}
	if len(params) != 0 {
		if params[0] != "" {
			dataParams := strings.Split(params[len(params)-1], ";")
			for _, v := range dataParams {
				temp := strings.Split(fmt.Sprintf("%s", v), ",")
				where := fmt.Sprintf("%s %s ?", strings.ToLower(temp[0]), temp[1])
				kolom = append(kolom, where)
				args = append(args, temp[2])
			}
		}
	}
	query := "SELECT * FROM usertb"

	dataKondisi := strings.Join(kolom, " AND ")
	if dataKondisi != "" {
		query += " WHERE " + dataKondisi
	}
	fmt.Println(query)
	fmt.Println(args)

	data, err := db.Query(query, args...)

	if err != nil {
		return nil, err
	}
	defer data.Close()
	var result []*Usertb
	for data.Next() {
		each := &Usertb{}
		err := data.Scan(&each.Id_user, &each.Username, &each.Password, &each.Nama, &each.Email, &each.T_lahir, &each.J_kelamin, &each.Level)
		if err != nil {
			return nil, err
		}
		result = append(result, each)
	}
	return result, nil
}

func (m *Usertb) Login(db *sql.DB) error {
	query := "SELECT * FROM usertb WHERE username = ? AND password =?"
	err := db.QueryRow(query, &m.Username, &m.Password).Scan(&m.Id_user, &m.Username, &m.Password, &m.Nama, &m.Email, &m.T_lahir, &m.J_kelamin, &m.Level)
	fmt.Println(m)
	return err
}

func (m *Usertb) GetLogin(db *sql.DB) error {
	query := "SELECT * FROM usertb WHERE id_user = ?"
	err := db.QueryRow(query, m.Id_user).Scan(&m.Id_user, &m.Username, &m.Password, &m.Nama, &m.Email, &m.T_lahir, &m.J_kelamin, &m.Level)
	return err
}

func GetsLogin(db *sql.DB, params ...string) ([]*Usertb, error) {
	var kolom = []string{}
	var args []interface{}
	if len(params) != 0 {
		if params[0] != "" {
			dataParams := strings.Split(params[len(params)-1], ";")
			for _, v := range dataParams {
				temp := strings.Split(fmt.Sprintf("%s", v), ",")
				where := fmt.Sprintf("%s %s ?", strings.ToLower(temp[0]), temp[1])
				kolom = append(kolom, where)
				args = append(args, temp[2])
			}
		}
	}
	dataKondisi := strings.Join(kolom, "AND")
	var query string
	query = "SELECT * FROM usertb"
	if dataKondisi != "" {
		query += "WHERE" + dataKondisi
	}
	datalog, err := db.Query(query, args...)

	if err != nil {
		return nil, err
	}
	defer datalog.Close()
	var result []*Usertb
	for datalog.Next() {
		each := &Usertb{}
		err := datalog.Scan(&each.Id_user, &each.Username, &each.Password, &each.Nama, &each.Email, &each.T_lahir, &each.J_kelamin, &each.Level)
		if err != nil {
			return nil, err
		}
		result = append(result, each)
	}
	return result, nil
}
