package main

import (
	"database/sql"
	"fmt"
	"kulit/crud/handler"
	"kulit/crud/lib"
	"kulit/crud/model"
	"net/http"
)

var (
	db              *sql.DB
	username        = "root"
	password        = ""
	database        = "kulit"
	databaseDefault = "mysql"
)

func main() {
	db, err := lib.ConnectMysql(database)
	fmt.Println(err)
	if err != nil {
		db, err = prepare()
		if err != nil {
			fmt.Println(err.Error())
		}
	}

	defer db.Close()
	handler.RegistDB(db)
	http.HandleFunc("/api/", handler.API)
	http.Handle("/", http.FileServer(http.Dir("frontend/user")))
	http.Handle("/panel/", http.StripPrefix("/panel/", http.FileServer(http.Dir("frontend/admin"))))
	http.ListenAndServe(":8087", nil)
}

func prepare() (*sql.DB, error) {
	fmt.Println("bluuuu")
	db, err := lib.ConnectMysql(databaseDefault)
	if err != nil {
		return nil, err
	}
	err = lib.CreateDB(db, database)
	if err != nil {
		return nil, err
	}

	db, err = lib.ConnectMysql(database)
	if err != nil {
		return nil, err
	}

	err = lib.CreateTable(db, model.TableGejalasakit)
	if err != nil {
		return nil, err
	}

	err = lib.CreateTable(db, model.TablePenyakit)
	if err != nil {
		return nil, err
	}

	err = lib.CreateTable(db, model.TableRiwayat)
	if err != nil {
		return nil, err
	}

	err = lib.CreateTable(db, model.TableUsertb)
	if err != nil {
		return nil, err
	}

	err = lib.CreateTable(db, model.TableInfo)
	if err != nil {
		return nil, err
	}

	err = lib.CreateTable(db, model.TableHasildetail)
	if err != nil {
		return nil, err
	}

	err = lib.CreateTable(db, model.TableHasil)
	if err != nil {
		return nil, err
	}
	return db, nil
}
